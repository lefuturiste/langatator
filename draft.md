
# Evaluator (draft)

EDIT: that's not actually quite how I implemented the evaluator.

Map

componentList:
bytes steam
first byte is a uint8 representing the type of the component
then depending on the type of the component there is 0 or N bytes

components types:
- name: integer
  size: 4 bytes (for int)
- name: open parenthesis
  size: 0 bytes
- name: close parenthesis
  size: 0 bytes
- name: float
  size: 32 bytes (IEEE 754)
- name: operator
  size: 1 byte (255 operators are possible)
- name: function designator
  desc: id of a function
  size: 2 bytes (65536 functions are possibles)

Example map of `9+(5*(8+6))`

lexed stream
-TYPE:NUMERAL
VALUE:9
-TYPE:OPERATOR
VALUE:PLUS
-TYPE:OPEN_PARENTHESIS
VALUE:NULL
-TYPE:NUMERAL
VALUE:5
-TYPE:OPERATOR
VALUE:TIMES
-TYPE:OPEN_PARENTHESIS
VALUE:NULL
-TYPE:NUMERAL
VALUE:8
-TYPE:OPERATOR
VALUE:PLUS
-TYPE:NUMERAL
VALUE:6
-TYPE:CLOSE_P
VALUE:NULL
-TYPE:CLOSE_P
VALUE:NULL

scan the whole lexed stream

So there will be a kind of time line with reference, a list of reference

in the begining we allocate two lists, one for the component type, one for the component values


## Dynamics lists

we allocate N bytes (uint_8 list[2048])

methods:

list_length()
list_assign_int(int index, int value)
list_assign_float(int index, float value)
list_assign_uint_8(int index, uint_8 value)
list_get_int(int index)

etc...




