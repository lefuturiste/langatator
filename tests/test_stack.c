#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "../src/stack.h"
#include "../src/utils.h"

void test_stack()
{
    printf_wrap("== test stack == \n");
    int res;
    
    struct IntStack* stack1 = int_stack_init();
    assert(int_stack_push(stack1, 255));
    assert(1 == int_stack_length(stack1));
    assert(int_stack_push(stack1, 742));
    assert(2 == int_stack_length(stack1));

    assert(int_stack_pop(stack1, &res));
    assert(742 == res);
    assert(1 == int_stack_length(stack1));

    assert(int_stack_push(stack1, 0));
    assert(int_stack_push(stack1, -1));
    assert(int_stack_push(stack1, -2));
    assert(int_stack_push(stack1, 854));
    assert(int_stack_push(stack1, 1024));
    assert(int_stack_push(stack1, 2024));
    assert(int_stack_push(stack1, 2025));
    assert(int_stack_push(stack1, 2026));
    assert(16 == stack1->allocated);
    printf_wrap("%d \n", int_stack_length(stack1));
    assert(9 == int_stack_length(stack1));

    int_stack_print(stack1);

    assert(int_stack_pop(stack1, &res));
    assert(2026 == res);
    assert(8 == int_stack_length(stack1));
}