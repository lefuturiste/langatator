#include "../src/config.h"
#include "../src/utils.h"
#include "./test_utils.h"
#include "./test_evaluation.h"
#include "./test_line_processing.h"
#include "./test_stack.h"
#include "./test_var_store.h"
#include <stdio.h>

int main()
{
    int var1 = 0x34;
    printf_wrap("== UNIT TESTS == \n");
    test_utils();
    printf_wrap("ça commence à faire chier jpp\n");
    test_var_store();
    test_stack();
    test_evaluation();
    test_line_processing();
}
