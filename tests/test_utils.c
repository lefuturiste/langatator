#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "../src/number_parsing.h"
#include "../src/utils.h"

void test_utils()
{
    printf_wrap("== test utils == \n");

    // test int parsing
    char test1[] = "151087";
    int res = 0;
    parse_clean_positive_integer(strlen(test1), test1, &res);
    assert(res == 151087);

    char test2[] = "-428579";
    parse_int(test2, &res);
    assert(res == -428579);


    float res3 = 0;
    char test3[] = "3121.897";
    parse_float(test3, &res3);
    assert(float_almost_equal(3121.897, res3));

    float res4 = 0;
    char test4[] = "-0.4397";
    parse_float(test4, &res4);
    assert(float_almost_equal(-0.4397, res4));

    assert(integer_pow(1, 0) == 1);
    assert(integer_pow(2, 1) == 2);
    assert(integer_pow(2, 6) == 64);

    assert(m_factorial(0) == 1);
    assert(m_factorial(1) == 1);
    assert(m_factorial(2) == 2);
    assert(m_factorial(3) == 6);
    assert(m_factorial(4) == 2*3*4);

    assert(float_almost_equal(2.22, m_float_modulus(2.22, 10)));
    assert(float_almost_equal(28.619, m_float_modulus(200.22, 34.32)));

    assert(float_almost_equal(2.7182, m_exp(1.0)));
    assert(float_almost_equal(1, m_exp(0)));

    assert(float_almost_equal(0, m_sin(0)));
    assert(float_almost_equal(1, m_sin(CST_PI/2)));
    assert(float_almost_equal(0, m_sin(CST_PI)));
    assert(float_almost_equal(-1, m_sin(3*CST_PI/2)));
    assert(float_almost_equal(0, m_sin(2*CST_PI)));
    assert(float_almost_equal(0, m_sin(4*CST_PI)));

    assert(float_almost_equal(1.41421, m_sqrt(2)));
    assert(float_almost_equal(2.2360, m_sqrt(5)));

    assert(float_almost_equal(0, m_ln(1)));
    assert(float_almost_equal(1, m_ln(2.7182818)));

    // char src[] = "  hello world  ";
    // char* trimed = trim_space(src);
    // assert(strcmp("hello world", trimed) == 0);

    // char src2[] = "hello";
    // char* trimed2 = trim_space(&src2);
    // assert(strcmp("hello", trimed2) == 0);
    
    char src[] = "   termination   ";
    char dst[strlen(src)];
    trim_space((char*) &dst, src);
    assert(strcmp("termination", dst) == 0);

    char src2[] = "termination";
    char dst2[strlen(src2)];
    trim_space((char*) &dst2, src2);
    assert(strcmp("termination", dst2) == 0);

    
}