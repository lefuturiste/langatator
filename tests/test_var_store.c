#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "../src/var_store.h"

void test_var_store()
{
    printf_wrap("== test var store == \n");

    struct VariableStore* store = var_store_init();

    // hash function test
    int hash1, hash2, hash3 = 0;
    hash1 = var_store_hash_name(store, "a");
    hash2 = var_store_hash_name(store, "a");
    hash3 = var_store_hash_name(store, "a");
    assert(hash1 == hash2);
    assert(hash2 == hash3);
    assert(hash1 == hash3);

    hash1 = var_store_hash_name(store, "i");
    hash2 = var_store_hash_name(store, "i");
    hash3 = var_store_hash_name(store, "i");
    assert(hash1 == hash2);
    assert(hash2 == hash3);
    assert(hash1 == hash3);

    var_store_print(store);

    int originalLength = store->length;
    int varEx = 42;
    int originalHash = var_store_hash_name(store, "i");

    var_store_set(store, "i", TYPE_INT, &varEx);
    assert(originalHash == var_store_hash_name(store, "i"));
    assert(TYPE_INT == var_store_get_type(store, "i"));

    assert(originalLength+1 == store->length);
    assert(42 == var_store_get_int(store, "i"));

    // sequential assignement
    int iDest = 0;
    for (int i = 0; i < 10; i++) {
        var_store_set(store, "i", TYPE_INT, &i);
        var_store_copy(store, "i", (void*) &iDest);
        assert(i == iDest);
    }
}
