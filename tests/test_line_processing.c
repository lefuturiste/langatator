#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "../src/types.h"
#include "../src/number_parsing.h"
#include "../src/utils.h"
#include "../src/evaluator.h"
#include "../src/state.h"
#include "../src/var_store.h"
#include "../src/line_processing.h"

void test_line_processing()
{
    printf_wrap("== test line processing == \n");

    // test string manipulation
    assert(!recognize_word("", "hello"));
    assert(!recognize_word(" hell", "hello"));
    assert(recognize_word("hello", "hello"));
    assert(recognize_word("hello ", "hello"));
    assert(recognize_word(" hello ", "hello"));
    assert(recognize_word(" hello ", "hello"));
    assert(recognize_word("    hello  ", "hello"));

    assert(recognize_termination_keyword("end", "01 end", 2));
    assert(!recognize_termination_keyword("end", "01 end == something end", 2));
    assert(!recognize_termination_keyword("end", "xend", 0));
    assert(!recognize_termination_keyword("end", "end", 0));
    assert(recognize_termination_keyword("end", " end", 0));

    struct SetStatement res;
    assert(recognize_set_statement("set x to 1", &res));
    assert(recognize_set_statement("set someBigThings_yes to (wow)+yes", &res));
    assert(!recognize_set_statement("set  to ", &res));
    assert(!recognize_set_statement("set thing uo to yes", &res));
    assert(recognize_set_statement("set varToThing_to to yes", &res));

    struct IfStatement resIf;
    assert(recognize_if_statement("if something is not cool then", &resIf));
    assert(recognize_if_statement("if some_then_var then", &resIf));

    struct WhileStatement resWhile;
    assert(recognize_while_statement("while i < 10 do", &resWhile));
    assert(recognize_while_statement("while !(i = 10) do", &resWhile));
    assert(!recognize_while_statement(" while 1 then", &resWhile));

    struct StateContainer* state1 = state_init();

    assert(process_line(state1, "#"));
    assert(process_line(state1, "# some random comment"));
    assert(process_line(state1, "set VAR_A to 8.5"));
    printf_wrap("%d \n", var_store_get_int(state1->varStore, "VAR_A"));
    assert(float_almost_equal(8.5, var_store_get_float(state1->varStore, "VAR_A")));

    assert(process_line(state1, "set VAR_B to 1.5"));
    assert(float_almost_equal(1.5, var_store_get_float(state1->varStore, "VAR_B")));

    assert(process_line(state1, "set VAR_C to 142"));
    assert(142 == var_store_get_int(state1->varStore, "VAR_C"));

    assert(process_line(state1, "set VAR_D to VAR_A+VAR_B"));
    assert(float_almost_equal(10, var_store_get_float(state1->varStore, "VAR_D")));

    assert(process_line(state1, "set VAR_D to VAR_A+VAR_B"));
    assert(float_almost_equal(10, var_store_get_float(state1->varStore, "VAR_D")));

    assert(process_line(state1, "if VAR_D = 19 & 1 then"));
    

    char* lines1 = "# hello world\n"
        "set x to 5\n"
        "set y to 1\n"
        "if (x+y) = 6 then\n"
        "   set res to (1024)\n"
        "end\n"
        "\n";
    struct StateContainer* state2 = state_init();
    process_script(state2, lines1);
    
    assert(!state2->skipping);
    assert(1024 == var_store_get_int(state2->varStore, "res"));
    assert(5 == var_store_get_int(state2->varStore, "x"));
    assert(1 == var_store_get_int(state2->varStore, "y"));

    char* lines2 = "set x to 5\n"
        "set y to 1\n"
        "set z to -1\n"
        "if (x+y) = 6 then\n"
        "   if z = -1 then\n"
        "       set res to (1024)\n"
        "   end\n"
        "   set res to (res+1)\n"
        "end\n";
    process_script(state2, lines2);

    assert(!state2->skipping);
    assert(1025 == var_store_get_int(state2->varStore, "res"));
    
    char* lines3 = "set res to -42\n"
        "if 0 then\n"
        "   set res to 42\n"
        "end\n"
        "\n";
    process_script(state2, lines3);

    assert(!state2->skipping);
    assert(-42 == var_store_get_int(state2->varStore, "res"));

    char* lines4 = "set i to 0\n"
        "while !(i = 9) do\n"
        "   set i to i+1\n"
        "end\n";
    process_script(state2, lines4);

    assert(!state2->skipping);
    printf_wrap("got i: %d \n", var_store_get_int(state2->varStore, "i"));
    assert(9 == var_store_get_int(state2->varStore, "i"));

    // test break keyword
    char* lines5 = "set i to 0\n"
        "while 1 do\n"
        "   if (i = 10) then\n"
        "       break\n"
        "   end\n"
        "   set i to i+1\n"
        "end\n";
    process_script(state2, lines5);

    assert(!state2->skipping);
    printf_wrap("got i: %d \n", var_store_get_int(state2->varStore, "i"));
    assert(10 == var_store_get_int(state2->varStore, "i"));

    // test continue keyword
    char* lines6 = "set sum to 0\n"
        "set i to 0\n"
        "while 1 do\n"
        "   if (i = 5) then\n"
        "       set i to i+1\n"
        "       continue\n"
        "   end\n"
        "   if (i = 10) then\n"
        "       break\n"
        "   end\n"
        "   set sum to sum+1\n"
        "   set i to i+1\n"
        "end\n";
    process_script(state2, lines6);

    assert(!state2->skipping);
    assert(10 == var_store_get_int(state2->varStore, "i"));
    assert(9 == var_store_get_int(state2->varStore, "sum"));
}
