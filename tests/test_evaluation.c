#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "../src/types.h"
#include "../src/number_parsing.h"
#include "../src/utils.h"
#include "../src/evaluator.h"
#include "../src/state.h"

void test_evaluation()
{
    printf_wrap("== test evaluation == \n");

    struct StateContainer* state = state_init();

    // test int parsing
    int resVal = 0;
    unsigned char resType = 0;

    evaluate(state, "-4", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(-4 == resVal);

    evaluate(state, "  -    6   ", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(-6 == resVal);

    evaluate(state, " 1+ (-    45  ) ", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(-44 == resVal);

    evaluate(state, "-(4+9)+1", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(-12 == resVal);

    evaluate(state, "(-(8-9+5))+8", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(4 == resVal);

    evaluate(state, "2^6", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(64 == resVal);

    evaluate(state, "3 + 4", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(7 == resVal);

    evaluate(state, "(2*4)+0+0", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(8 == resVal);

    evaluate(state, "2.5-(2+0.1)", &resVal, &resType);
    assert(resType == TYPE_FLOAT);
    assert(float_almost_equal(0.4, get_float_from_int_rep(resVal)));

    evaluate(state, "1^0 +  (7*(5 +2))", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(50 == resVal);

    evaluate(state, "- ( 0.1+ 0.3 )", &resVal, &resType);
    assert(resType == TYPE_FLOAT);
    assert(float_almost_equal(-0.4, get_float_from_int_rep(resVal)));

    /* FUNCTIONS */

    evaluate(state, "sqrt(2)-1", &resVal, &resType);
    assert(resType == TYPE_FLOAT);
    assert(float_almost_equal(0.41421, get_float_from_int_rep(resVal)));

    evaluate(state, "(abs((0-1)*2)) + abs(2)", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(4 == resVal);

    evaluate(state, "exp(2)-1", &resVal, &resType);
    assert(resType == TYPE_FLOAT);
    assert(float_almost_equal(6.389, get_float_from_int_rep(resVal)));
    
    evaluate(state, "(cos(2)^2)+(sin(2)^2)", &resVal, &resType);
    assert(resType == TYPE_FLOAT);
    assert(float_almost_equal(1, get_float_from_int_rep(resVal)));

    evaluate(state, "ceil(1.5)", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(2 == resVal);

    evaluate(state, "floor(1.5)", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(1 == resVal);

    evaluate(state, "floor(1.001)", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(1 == resVal);
    
    evaluate(state, "random_int(1, 100)", &resVal, &resType);
    assert(resType == TYPE_INT);
    printf_wrap(" - random int: %d \n", resVal);

    evaluate(state, "abs(2)+abs(-2)", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(4 == resVal);

    evaluate(state, "max(25, 4)", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(25 == resVal);

    evaluate(state, "max(-25, 42)", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(42 == resVal);

    evaluate(state, "max( (1+1)^(6+1-1) , 4+2)", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(64 == resVal);

    evaluate(state, "max(floor(exp(1))^(4+1) , -2)", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(32 == resVal);

    // testing function composition is important
    evaluate(state, "abs(abs(-2))", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(2 == resVal);

    /* COMPARAISON OPERATORS */
    evaluate(state, "2 = 2", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(1 == resVal);

    evaluate(state, "2 = 3", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(0 == resVal);

    evaluate(state, "2 = 3.2", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(0 == resVal);

    evaluate(state, "!(2 = 3)", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(1 == resVal);

    evaluate(state, "!! 0", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(0 == resVal);

    evaluate(state, "(! (! 1)) + 0", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(1 == resVal);

    evaluate(state, "!!2", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(1 == resVal);

    evaluate(state, "1 & 1", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(1 == resVal);

    evaluate(state, "1 & ((1+0)-1)", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(0 == resVal);

    evaluate(state, "1 & (1 | 0)", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(1 == resVal);

    evaluate(state, "0 > 0", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(0 == resVal);

    evaluate(state, "-45 < 1", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(1 == resVal);

    evaluate(state, "5 > 0", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(1 == resVal);

    evaluate(state, "5 % 10", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(5 == resVal);

    evaluate(state, "5.3 % 10", &resVal, &resType);
    assert(resType == TYPE_FLOAT);
    assert(float_almost_equal(5.3, get_float_from_int_rep(resVal)));

    /* VARIABLES */
    int ex = 43;
    var_store_set(state->varStore, "var", TYPE_INT, (void*) &ex);
    evaluate(state, "var", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(43 == resVal);

    evaluate(state, "var < 52", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(1 == resVal);

    evaluate(state, "var < (52-2)", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(1 == resVal);

    int ex2 = 3;
    var_store_set(state->varStore, "var2", TYPE_INT, (void*) &ex2);

    evaluate(state, "abs((var2^2)-((var-41)^2))+2", &resVal, &resType);
    assert(resType == TYPE_INT);
    printf_wrap("actually got: %d \n", resVal);
    assert(7 == resVal);

    /* TYPE FUNCS */
    evaluate(state, "is_number(123)", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(resVal);

    evaluate(state, "is_number(-25.5)", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(resVal);

    evaluate(state, "is_number(NULL)", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(!resVal);

    evaluate(state, "is_null(NULL)", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(resVal);

    evaluate(state, "(is_null(NULL) & is_number(42)) | 0", &resVal, &resType);
    assert(resType == TYPE_INT);
    assert(resVal);
}
