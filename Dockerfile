FROM alpine:3.14
RUN apk add gcc
RUN apk add make
RUN mkdir /app
COPY ./ /app
ENTRYPOINT ["sleep", "infinity"]
