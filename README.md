# Langatator

A very basic interpreted imperative programming language.

## Background

The goal of this project is to create a simple implementation of a BASIC-like language just to learn a few things along the way and practice my C programming skills.

No need to do complex things, just to create a simple interpreted language that can be used to do some arithmetics and create for example a number guessing game.

I didn't really study how others languages works beforehand, I'm just guessing how I'm implementing things so that I can make mistakes to learn from.

## Progress

- 2022-04-29 Implementation of a basic evaluator engine to evaluate arithmetic expressions and call functions
- 2022-05-15 Clear progress being able to use simple while and if though thre are many glitches

ToDo List:

- [X] feat: pow operator
- [X] feat: binary operators
- [X] feat: basic math functions
- [X] feat: random_int(min, max)
- [X] feat: print_number(message)
- [X] base of unit tests
- [X] feat: set variables
- [X] feat: read line comments
- [X] feat: modulus operator '%'
- [X] feat: input_number() std function
- [X] feat: NULL type (dirty way to handle no returns and some errors)
- [X] feat: type() std function and others type checking functions
- [X] feat: ceil() and floor() std functions
- [X] feat: base of the CLI
- [X] feat: process from a file
- [X] feat: if statements
- [X] feat: while statements (with break and continue)

- [ ] feat(Evaluator): multiple characters operators
- [ ] feat(Evaluator): inclusive operators like '!=', '>=', '<='
- [ ] feat: functions support
- [ ] feat(Evaluator): priority operators
- [ ] feat: multiline expressions
- [ ] feat: short hand if statement without 'end'
- [ ] feat: repeat statement
- [ ] feat: static string support (just for ui)
- [ ] feat: print_string function
- [ ] feat: basic number list support
- [ ] feat: fully features strings support
- [ ] feat: function to access to environment variables
- [ ] feat: hexadecimal and binary constants

- [X] feat: REPL environment
- [ ] feat: add history to REPL
- [ ] evaluate expression from stdin
- [X] feat: config header file
- [X] ability to modify keywords and customize the lang
- [ ] more config options

- [ ] add [classic problem solving](https://rosettacode.org) with code examples
- [ ] feat: web Assembly support and publish a demo website

- [ ] refactor: add 'Literal' struct with type and data
- [ ] refactor(Evaluator): if branching around token identification
- [ ] refactor(List): use malloc
- [ ] refactor: remove inconsistency on how functions returns error codes

## Installation

You will need to compile the code from source.

- Clone this repository
- Then compile (eg. with `make`)

I use GNU Make with GCC, but I'm sure you can use any C compilers though you may need to edit some part of the code to cope with other compilers (eg. binary constants).

## Build to Web assembly

I use emscripten.org

It would be great if I'm not using emscripten.

I have to find implementation for stdlib

## Unit testing

I try to have some sort of code coverage, you can run the unit tests by issuing `make test`

## The language

You would be able to use the lang directly via CLI, via a REPL or by writing in a file (file ext `.ltor`).

One instruction set per line.

### Comments

Can only use single line comments with `#`

```
# this is a comment
```

### Data types

To begin with and order to simplify things we would only have numbers as datatypes.

When a variable is declared it can be 32 bit integer or 32 bit float.

The language may support strings literals in the future.

### Expression evaluation

function calls: func(arg_a, arg_b)
operators: +, *, /, ^, %, =, <, >, &, |, !

### Set a variable

```
set {VARNAME} to {EXPRESSION}
```

### Evaluate an expression without using the result

```
print_number(42)
```

### function definition

```
function {NAME} do
  ...
end
```

### Conditional structure

```
if {EXPRESSION} then
  ...
end
```

### Conditional loop

```
while {EXPRESSION} do
  ...
end
```

### Unconditional loop

```
repeat {INT EXPRESSION} do
  ...
end
```

```
repeat i from {INT EXPRESSION} to {INT EXPRESSION} do
  ...
end
```

### std functions

```
abs(nb)
sqrt,sin,cos,exp,ln,log etc.
print_number(nb)
input_number()
ceil(nb)
floor(nb)
random_int(min, max)
random_float(min, max)
type(var) -> return the type of a var as int
is_null(var)
is_number(var)
print_string(str)
print_newline()
print_ascii(nb)
```


