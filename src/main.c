#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include "./config.h"
#include "./utils.h"
#include "./types.h"
#include "./stack.h"
#include "./list.h"
#include "./number_parsing.h"
#include "./evaluator.h"
#include "./state.h"
#include "./var_store.h"
#include "./line_processing.h"

/**
This is the binding of the interpreter to a CLI
The interpreter should be uncoupled from the rest of the code
*/

#define HELP_FLAG        1
#define VERSION_FLAG     4
#define INTERACTIVE_FLAG 8
#define STDIN_EXP_FLAG   16

int help_mode(char* cmdName) {
    char* helpStr = "Usage: %s [options] [file]\n"
        "  -h  --help              print this usage and exit\n"
        "  -v  --version           print version information and exit\n"
        "  -i  --interactive       force interactive mode\n"
        "  -e  --stdin-expression  evaluate expression from stdin\n";
    printf_wrap(helpStr, cmdName);
    return 0;
}

int version_mode() {
    char* helpStr = "Langatator 0.0.1\n";
    printf_wrap(helpStr);
    return 0;
}

int interactive_mode() {
    struct StateContainer* state = state_init();

    while (state->running) {
        printf_wrap("? ");

        char* line;
        size_t len = 0;
        size_t lineSize = 0;

        lineSize = getline(&line, &len, stdin);
        if (lineSize == (size_t) -1) {
            // we received some kind of interrupt?
            printf_wrap("\n");
            return EXIT_SUCCESS;
        }

        char toEvaluate[lineSize+1];
        // remove the new line at the end
        str_extract((char*) toEvaluate, line, 0, lineSize-1);

        int stat = process_line(state, toEvaluate);
        if (!stat) {
            printf_wrap("Processing that line failed.\n");
        }
        if (state->lastEvaluationType != TYPE_NULL) {
            printf_wrap("%s\n", get_repr(state->lastEvaluationType, &state->lastEvaluationResult));
        }
    }

    return EXIT_SUCCESS;
}

int stdin_expression_mode() {
    printf_wrap("This mode is not implemented yet ¯\\_(ツ)_/¯ \n");
    return 1;
}


char* slurp_file(char* filePath, size_t* size) {
    char* buffer = NULL;

    FILE* f = fopen(filePath, "rb");
    if (f == NULL) goto error;

    if (fseek(f, 0, SEEK_END) < 0) goto error;

    long m = ftell(f);
    if (m < 0) goto error;

    buffer = malloc(sizeof(char) * m);
    if (buffer == NULL) goto error;

    if (fseek(f, 0, SEEK_SET) < 0) goto error;

    size_t n = fread(buffer, 1, m, f);
    assert(n == (size_t) m);

    if (ferror(f)) goto error;

    if (size) *size = n;

    fclose(f);

    return buffer;

error:
    if (f) fclose(f);

    if (buffer) free(buffer);

    return NULL;
}

int file_mode(char* fileName) {
    // printf_wrap("Open file mode...\n");
    size_t size;
    char* buff = slurp_file(fileName, &size);

    if (!buff) {
        fprintf(stderr, "Error: Cannot load file '%s' errno: %d '%s'.\n", fileName, errno, strerror(errno));
        return EXIT_FAILURE;
    }

    struct StateContainer* state = state_init();
    process_script(state, buff);

    return EXIT_SUCCESS;
}

int main (int argc, char** argv) {
    int flags = INTERACTIVE_FLAG;

    int nonFlagArgumentCount = 0;
    int nonFlagArgumentPos = 0;

    char* cmdName = argv[0];
    for (int i = 1; i < argc; i++) {
        if (str_starts_with("--help", argv[i]) || str_starts_with("-h", argv[i])) {
            flags = HELP_FLAG;
            break;
        }
        if (str_starts_with("--version", argv[i]) || str_starts_with("-v", argv[i])) {
            flags = VERSION_FLAG;
            break;
        }
        if (str_starts_with("--interactive", argv[i]) || str_starts_with("-i", argv[i])) {
            flags = INTERACTIVE_FLAG;
            break;
        }
        if (str_starts_with("--stdin-expression", argv[i]) || str_starts_with("-e", argv[i])) {
            flags = STDIN_EXP_FLAG;
            break;
        }
        // no flag match
        if (str_starts_with("--", argv[i])) {
            fprintf(stderr, "Invalid flag '%s'\n", argv[i]);
            return EXIT_FAILURE;
        }
        nonFlagArgumentCount++;
        nonFlagArgumentPos = i;
    }
    if (nonFlagArgumentCount == 1) return file_mode(argv[nonFlagArgumentPos]);

    if (flags & HELP_FLAG) return help_mode(cmdName);
    if (flags & VERSION_FLAG) return version_mode();
    if (flags & INTERACTIVE_FLAG) return interactive_mode();
    if (flags & STDIN_EXP_FLAG) return stdin_expression_mode();

    help_mode(cmdName);
    return EXIT_FAILURE;
}
