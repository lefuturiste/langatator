#include "./utils.h"
#include "./var_store.h"
#include "./state.h"
#include <stdlib.h>

struct StateContainer* state_init()
{
    struct StateContainer* ptr = (struct StateContainer*) malloc(sizeof(struct StateContainer));
    ptr->varStore = var_store_init();

    ptr->linePtr = 0;

    ptr->loopStack = int_stack_init();
    ptr->blockStack = int_stack_init();
    
    ptr->blockStackAnchor = 0;
    ptr->skipping = 0;

    ptr->running = 1;

    ptr->lastEvaluationType = TYPE_NULL;
    ptr->lastEvaluationResult = 0;

    return ptr;
}
