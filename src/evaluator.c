#include "./evaluator.h"
#include <stdio.h>
#include <string.h>
#include "./types.h"
#include "./list.h"
#include "./operate.h"
#include "./number_parsing.h"
#include "./funcs.h"
#include "./var_store.h"
#include "./state.h"

/**
This sub script will look for pattern like Number Operator Number
and evaluate that
*/
int evaluator_reduce_operator_pattern(struct List* evalList) {
    int patternPos = -1;
    for (int i = 0; i < evalList->num_elements; i++) {
        if (
            is_type_number(list_get_type(evalList, i)) &&
            list_get_type(evalList, i+1) == TYPE_OPERATOR &&
            is_type_number(list_get_type(evalList, i+2))
        ) {
            patternPos = i;
            break;
        }
    }

    if (patternPos == -1) {
        // we did not find anything to reduce we return negative
        return -1;
    }
    // we found a positive pattern, we gonna reduce
    int typeA = list_get_type(evalList, patternPos);
    int typeB = list_get_type(evalList, patternPos+2);
    
    // que ça soit un float ou un int, ça tient en 32 bit donc dans un int :p
    int a;
    char operator;
    int b;

    list_get(evalList, patternPos, &a);
    list_get(evalList, patternPos+1, &operator); // id de l'operateur
    list_get(evalList, patternPos+2, &b);
    
    int res = 0;
    byte typeRes = 0;
    int operateStatus = operate(operator, typeA, a, typeB, b, &typeRes, &res);
    if (operateStatus != 0) {
        // the pattern that matched, did not worked as expected
        // this is a failure
        printf_wrap("ERR Evaluator: cannot operate \n");
        return 400;
    }

    if (typeRes == TYPE_INT) {
        list_set(evalList, patternPos, TYPE_INT, &res);
    }
    if (typeRes == TYPE_FLOAT) {
        // FIXME: invalid type set, we need to flag for float
        list_set(evalList, patternPos, TYPE_FLOAT, &res);
    }

    list_delete(evalList, patternPos+1);
    list_delete(evalList, patternPos+1);

    //printf_wrap("END OF THE FIRST OPERATION \n");
    //list_print(&evalList);
    
    // so we reduced an expression, return 0
    return 0;
}

/**
This sub script will look for pattern like Operator (minus sign) Number
and evaluate that
*/
int evaluator_reduce_minus_pattern(struct List* evalList) {
    int patternPos = -1;
    for (int i = 0; i < evalList->num_elements; i++) {
        if (
            ((i >= 1 && !is_type_number(list_get_type(evalList, i-1))) || (i == 0)) &&
            ((i >= 1 && list_get_type(evalList, i-1) != TYPE_CLOSE_PARENTHESIS) || (i == 0)) &&
            ((i >= 1 && list_get_type(evalList, i-1) != TYPE_VAR_NAME) || (i == 0)) &&
            list_get_type(evalList, i) == TYPE_OPERATOR &&
            is_type_number(list_get_type(evalList, i+1))
        ) {
            char candidateOperator;
            list_get(evalList, i, &candidateOperator);
            if (candidateOperator != '-') continue;

            patternPos = i;
            break;
        }
    }

    if (patternPos == -1) return -1;

    // we found a positive pattern, we gonna reduce
    int type = list_get_type(evalList, patternPos+1);
    int val;
    list_get(evalList, patternPos+1, &val);
    
    int res = 0;
    unsigned char typeRes = 0;
    int operateStatus = operate('*', TYPE_INT, -1, type, val, &typeRes, &res);
    if (operateStatus != 0) {
        printf_wrap("ERR Evaluator: cannot operate \n");
        return 400;
    }

    if (typeRes == TYPE_INT) {
        list_set(evalList, patternPos, TYPE_INT, &res);
    }
    if (typeRes == TYPE_FLOAT) {
        // FIXME: invalid type set, we need to flag for float
        list_set(evalList, patternPos, TYPE_FLOAT, &res);
    }

    list_delete(evalList, patternPos+1);

    return 0;
}

int evaluator_reduce_not_pattern(struct List* evalList) {
    int patternPos = -1;
    for (int i = 0; i < evalList->num_elements; i++) {
        if (
            list_get_type(evalList, i) == TYPE_OPERATOR &&
            is_type_number(list_get_type(evalList, i+1))
        ) {
            char candidateOperator;
            list_get(evalList, i, &candidateOperator);
            if (candidateOperator != '!') continue;

            patternPos = i;
            break;
        }
    }

    if (patternPos == -1) return -1;

    int type = list_get_type(evalList, patternPos+1);
    int val;
    list_get(evalList, patternPos+1, &val);
    
    int res = 0;
    byte typeRes = 0;
    int operateStatus = operate('!', 0, 0, type, val, &typeRes, &res);
    if (operateStatus != 0) {
        printf_wrap("ERR Evaluator: cannot operate \n");
        return 400;
    }

    if (typeRes != TYPE_INT) return 300;

    list_set(evalList, patternPos, typeRes, &res);
    list_delete(evalList, patternPos+1);

    return 0;
}

/**
This sub script will look for pattern like ({Number})
and remove parenthesis
*/
int evaluator_reduce_parenthesis_pattern(struct List* evalList) {
    int patternPos = -1;
    for (int i = 0; i < evalList->num_elements; i++) {
        if (
            ((i >= 1 && list_get_type(evalList, i-1) != TYPE_FUNC_NAME) || (i == 0)) &&
            list_get_type(evalList, i) == TYPE_OPEN_PARENTHESIS &&
            is_type_literal(list_get_type(evalList, i+1)) &&
            list_get_type(evalList, i+2) == TYPE_CLOSE_PARENTHESIS
        ) {
            patternPos = i;
            break;
        }
    }

    if (patternPos == -1) {
        // we did not find anything to reduce we return -1
        return -1;
    }
    // we found a positive pattern position, we gonna reduce that
    // so we just have to remove the two parenthesis
    list_delete(evalList, patternPos);
    list_delete(evalList, patternPos+1);
    
    // so we reduced an expression, return 0
    return 0;
}

int evaluator_reduce_var(struct StateContainer* state, struct List* evalList) {
    int patternPos = -1;
    for (int i = 0; i < evalList->num_elements; i++) {
        if (
            list_get_type(evalList, i) == TYPE_VAR_NAME
        ) {
            patternPos = i;
            break;
        }
    }
    if (patternPos == -1) return -1;

    int varKey;
    list_get(evalList, patternPos, &varKey);

    byte type = var_store_get_type_from_key(state->varStore, varKey);

    if (EVALUATOR_DEBUG_LEVEL >= 2) printf_wrap("Going to reduce var key %d at pos %d\n", varKey, patternPos);
    
    byte varVal[get_size_of_type(type)];
    var_store_copy_from_key(state->varStore, varKey, &varVal);

    list_set(evalList, patternPos, type, &varVal);
    
    return 0;
}

/*
This func will look for a function call pattern that is ready to be evaluated
First we will look for this kind of pattern
FUNC_NAME
OPEN_PARENTHESIS
LITERAL VALUE (for now just NUMBER_TYPE)

but no CLOSE_PARENTHESIS

if we found this pattern, we will then try to find the potential remaining arguments
like check if we have COMMA ARGUMENT
if we don't find a comma next, we try to find a closing parenthesis
if we don't find that, we throw an error

*/
int evaluator_reduce_function_call(struct List* evalList, int initialPosition) {
    int patternPos = -1;

    if (initialPosition >= evalList->num_elements) {
        return -1;
    }

    for (int i = initialPosition; i < evalList->num_elements; i++) {
        if (
            list_get_type(evalList, i) == TYPE_FUNC_NAME &&
            list_get_type(evalList, i+1) == TYPE_OPEN_PARENTHESIS
        ) {
            // mode: we have a function call
            patternPos = i;
            break;
        }
    }

    if (patternPos == -1) {
        // we did not find anything to reduce we return -1
        return -1;
    }

    short funcID;
    // fetch the function ID
    list_get(evalList, patternPos, &funcID);

    short argsLen = 0;
    int pos = patternPos+2;

    // now we know we have a start of a function
    // we gonna start to look if there is others arguments
    // two case: either we have another arguments or we have a closing parenthesis
    while (1) {
        if (argsLen > 16) {
            printf_wrap("ERR Evaluator: too many arguments passed to func (max out the limit) \n");
            return 100;
        }
        if (
            list_get_type(evalList, pos) == TYPE_CLOSE_PARENTHESIS
        ) {
            // end of the function call
            pos += 0;
            break;
        }
        if (
            list_get_type(evalList, pos) == TYPE_COMMA &&
            list_get_type(evalList, pos+1) == TYPE_CLOSE_PARENTHESIS
        ) {
            // end of the function call
            pos += 1;
            break;
        }
        
        if (
            is_type_literal(list_get_type(evalList, pos)) &&
            list_get_type(evalList, pos+1) == TYPE_COMMA
        ) {
            // this is a new argument
            pos += 2;
            argsLen++;
            continue;
        }
        if (
            is_type_literal(list_get_type(evalList, pos)) &&
            list_get_type(evalList, pos+1) == TYPE_CLOSE_PARENTHESIS
        ) {
            // we didn't match the last pattern so it's the last argument
            pos += 1;
            argsLen++;
            continue;
        }

        // we cannot reduce more, it's probably because the func call is not ready to be reduced, meaning that the arguments need to be evaluated
        
        // we need to return to the state of primitive function detection, just skip this primitive
        // we need to revaluate at patternPos+2
        return evaluator_reduce_function_call(evalList, patternPos+2);
    }
    // now pos is the index of the last component for this func call

    unsigned char argsTypes[argsLen];
    int argsValues[argsLen];

    // populate the arguments types and values
    for (int i = 0; i < argsLen; i++) {
        argsTypes[i] = list_get_type(evalList, patternPos+2+(i*2));
        list_get(evalList, patternPos+2+(i*2), &argsValues[i]);
    }

    // now call the funcs
    int resVal = 0;
    unsigned char resType = 0;
    int stat = execute_func(funcID, argsLen, argsTypes, argsValues, &resVal, &resType);

    if (stat != 0) {
        // error when executing the func
        return 1;
    }

    // now we can delete in the list from pos+1 patternPos to pos
    // just delete N-1 times where N is the number of components in the func call
    //printf_wrap("start: %d, end: %d \n", patternPos, pos);
    //printf_wrap("patternPos: %d, pos: %d \n", patternPos, pos);
    for (int j = 0; j < (pos-patternPos); j++) {
        list_delete(evalList, patternPos);
    }

    //printf_wrap("list report after deleting after applying a func \n");
    //list_print(evalList);

    //printf_wrap("patternPos: %d, resType: %d \n", patternPos, resType);
    list_set(evalList, patternPos, resType, &resVal);


    return 0;
}

/*
Evaluate a simple string expression return 0 if no fails occurs
And return smth different than 0 if there is an error with the lexing/parsing/evaluation
Arguments:
- Input String: char pointer (the source of the evaluation)
- Result: int pointer (where the result of the evaluation will be written)
*/
// FIXME: allow for multiple line expressions
int evaluate(struct StateContainer* state, char* inputStr, int* resultPtr, unsigned char* typePtr) {
    int i = 0;
    int _len = strlen(inputStr);
    // we want first to parse the expression and create a stack
    // for 4*81 : ["4", "*", "81"]
    // for -4*(5+42) : ["-4", "*", "(", "5", "+", "42", ")"]
    // as a matter of fact, this is a partition
    // so we can treat that as a list of tuple (start,finish)
    int partitionStartPos[_len];
    int partitionStopPos[_len];
    int partitionPtr = 0;
    int lastStartPos = 0;
    while (inputStr[i]  != 0) {
        if (
            is_operator(inputStr[i]) ||
            inputStr[i] == '(' ||
            inputStr[i] == ')' ||
            inputStr[i] == ','
        ) {
            if (lastStartPos != i) {
                partitionStartPos[partitionPtr] = lastStartPos;
                partitionStopPos[partitionPtr] = i;
                partitionPtr++;
            }

            partitionStartPos[partitionPtr] = i;
            partitionStopPos[partitionPtr] = i+1;
            partitionPtr++;

            lastStartPos = i+1;
            i++;
            continue;
        }
        i++;
    }
    if (lastStartPos != i) {
        partitionStartPos[partitionPtr] = lastStartPos;
        partitionStopPos[partitionPtr] = i;
        partitionPtr++;
    }

    // display the partition
    if (EVALUATOR_DEBUG_LEVEL >= 2) {
        printf_wrap("partitionPtr: %d \n", partitionPtr);
    }
    for (int j = 0; j < partitionPtr; j++) {
        if (EVALUATOR_DEBUG_LEVEL >= 2) {
            printf_wrap("start %d ", partitionStartPos[j]);
            printf_wrap("stop %d ", partitionStopPos[j]);
        }
        int len = partitionStopPos[j] - partitionStartPos[j];
        
        char buff[_len];
        for (int z = 0; z < len; z++) {
            if (z < len) {
                buff[z] = inputStr[partitionStartPos[j]+z];
            }
        }
        buff[len] = 0;
        if (EVALUATOR_DEBUG_LEVEL >= 2) {
            printf_wrap("content %s \n", buff);
        }
    }

    // once we've lexed our expression, we want to analyse if it's righly parenthesized
    int parenthesisCount = 0;
    for (int j = 0; j < partitionPtr; j++) {
        int len = partitionStopPos[j] - partitionStartPos[j];
        char buff[_len];
        for (int z = 0; z < len; z++) {
            if (z < len) {
                buff[z] = inputStr[partitionStartPos[j]+z];
            }
        }
        buff[len] = 0;
        if (len == 1 && buff[0] == '(') {
            parenthesisCount++;
        }
        if (len == 1 && buff[0] == ')') {
            parenthesisCount--;
        }
        if (parenthesisCount < 0) {
            printf_wrap("ERR Evaluator: bad parenthesizing \n");
            return 100;
        }
    }

    if (parenthesisCount > 0) {
        // there is an open parenthesis, so it's an error
        printf_wrap("ERR Evaluator: invalid parenthesis stack \n");
        return 100;
    }

    struct List evalList;
    // NOTICE: for some reason the struct don't reset after a usage
    list_reset(&evalList);
    if (EVALUATOR_DEBUG_LEVEL >= 2) printf_wrap("\n - constructing list \n");
    // initializing the evaluation list
    for (int j = 0; j < partitionPtr; j++) {

        int startPos = partitionStartPos[j];
        int stopPos = partitionStopPos[j];
        if (EVALUATOR_DEBUG_LEVEL >= 2) {
            printf_wrap("=== %d\n", j);
            printf_wrap("startPos %d, stopPos %d\n", startPos, stopPos);
        }
        int len = stopPos - startPos;

        // modifiy the start and stop pos to trim spaces
        int startTrimOffset = 0;
        int stopTrimOffset = 0;
        for (int z = 0; z < len; z++) {
            if (inputStr[startPos+z] != ' ') break;
            startTrimOffset++;
        }
        for (int z = 1; z < len; z++) {
            if (inputStr[stopPos-z] != ' ')  break;
            stopTrimOffset++;
        }
        startPos += startTrimOffset;
        stopPos -= stopTrimOffset;
        
        len = stopPos - startPos;
        char buff[_len+1];
        buff[0] = 0;

        // fill the buffer with the component string
        for (int z = 0; z < len; z++) {
            buff[z] = inputStr[startPos+z];
        }
        buff[len] = 0; // terminate the buff

        // TODO: SPLIT INTO A FUNCTION "identify_token(char* str)"
        if (EVALUATOR_DEBUG_LEVEL >= 2) printf_wrap("buff: '%s' \n", buff);
        
        char dumbValue = (char) 0;

        if (buff[0] == '\0') continue;
        if (len == 1 && buff[0] == '(') {
            list_set(&evalList, evalList.num_elements, TYPE_OPEN_PARENTHESIS, &dumbValue);
            continue;
        }
        if (len == 1 && buff[0] == ')') {
            list_set(&evalList, evalList.num_elements, TYPE_CLOSE_PARENTHESIS, &dumbValue);
            continue;
        }
        if (len == 1 && buff[0] == ',') {
            list_set(&evalList, evalList.num_elements, TYPE_COMMA, &dumbValue);
            continue;
        }
        if (len == 1 && is_operator(buff[0])) {
            if (EVALUATOR_DEBUG_LEVEL >= 2) printf_wrap("found op\n");
            char opValue = buff[0];
            list_set(&evalList, evalList.num_elements, TYPE_OPERATOR, &opValue);
            continue;
        }

        int res = 0;
        int st = parse_int(buff, &res);
        if (st == 0) {
            // parse int success
            //printf_wrap("Content aftr parsing %d %d \n", st, res);
            list_set(&evalList, evalList.num_elements, TYPE_INT, &res);
        }
        if (st != 0) {
            // failed to parsed as an int, try to parse as a float
            st = parse_float(buff, (float*) &res);
            if (st == 0) {
                // parse float success
                list_set(&evalList, evalList.num_elements, TYPE_FLOAT, &res);
            }
        }

        // FIXME: refactor to better branching
        if (st != 0) {
            // identify token
            // first try a variable then a func name
            // not a float, check if this is a common function name
            if (EVALUATOR_DEBUG_LEVEL >= 2) printf_wrap("now going to identify token '%s' \n", buff);
            if (EVALUATOR_DEBUG_LEVEL >= 2) var_store_print(state->varStore);
            
            int varKey = (int) var_store_get_key(state->varStore, buff);
            if (EVALUATOR_DEBUG_LEVEL >= 2) printf_wrap("got var key '%d' \n", varKey);
            
            if (varKey == -1) {
                // did not find the var name
                short funcID = identify_func_name(buff);
                if (funcID == -1) {
                    // did not find the func name
                    printf_wrap("ERR Evaluator: could not identify token \"%s\" \n", buff);
                    return 200;
                }
                if (funcID >= 0) {
                    list_set(&evalList, evalList.num_elements, TYPE_FUNC_NAME, &funcID);
                }
            }
            if (varKey >= 0) {
                list_set(&evalList, evalList.num_elements, TYPE_VAR_NAME, &varKey);
            }
        }
        if (EVALUATOR_DEBUG_LEVEL >= 2) printf_wrap("end of a token identification\n");
    }

    // check the content of this thing
    if (EVALUATOR_DEBUG_LEVEL >= 2) {
        list_print(&evalList);
        printf_wrap("Now going to actually evaluate...\n");
    }

    while (evalList.num_elements > 1 || !is_type_literal(list_get_type(&evalList, 0))) {
        if (EVALUATOR_DEBUG_LEVEL >= 2) list_print(&evalList);

        // int reduceVarOpStat = evaluator_reduce_var(state, &evalList);
        // int reduceFuncOpStat = evaluator_reduce_function_call(&evalList, 0);
        // int reduceMinusOpStat = evaluator_reduce_minus_pattern(&evalList);
        // int reduceOperatorOpStat = evaluator_reduce_operator_pattern(&evalList);
        // int reduceParenthesisOpStat = evaluator_reduce_parenthesis_pattern(&evalList);

        // we are going to look for pattern to reduce
        // the order is really important here
        byte didReduced = 0;
        for (int m = 0; m < 6; m++) {
            short stat = 0;
            if (m == 0) {
                stat = evaluator_reduce_var(state, &evalList);
                if (stat == 0) {
                    didReduced = 1;
                    break;
                }
            }
            if (m == 1) stat = evaluator_reduce_function_call(&evalList, 0);
            if (m == 2) stat = evaluator_reduce_minus_pattern(&evalList);
            if (m == 3) stat = evaluator_reduce_not_pattern(&evalList);
            if (m == 4) stat = evaluator_reduce_operator_pattern(&evalList);
            if (m == 5) stat = evaluator_reduce_parenthesis_pattern(&evalList);
            if (stat > 0) {
                printf_wrap("ERR Evaluator: mode %d reducing failed \n", m);
                if (EVALUATOR_DEBUG_LEVEL >= 2) {
                    printf_wrap("dumping evalList: \n");
                    list_print(&evalList);
                }
            }
            if (stat == 0) didReduced = 1;
        }
        
        if (!didReduced) {
            // all scans failed to find things to reduce
            // this is actually a failure because we can't do anything to get down to 1 element in the eval list
            printf_wrap("ERR Evaluator: could not reduce more \n");
            if (EVALUATOR_DEBUG_LEVEL >= 2) {
                printf_wrap("dumping evalList: \n");
                list_print(&evalList);
            }
            return 400;
        }

    }
    int typeRes = list_get_type(&evalList, 0);
    *typePtr = typeRes;
    list_get(&evalList, 0, resultPtr);
    if (EVALUATOR_DEBUG_LEVEL >= 2) {
        printf_wrap("End of evaluation, dumping evalList: \n");
        list_print(&evalList);
    }

    return 0;
}
