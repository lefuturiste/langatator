#ifndef NB_PARSING_H_
#define NB_PARSING_H_

#define NB_PARSING_DEBUG_LEVEL 0

int is_char_numeral(char candidate);
int integer_pow(int base, int exponent);
int parse_clean_positive_integer(int inputStrLen, char* inputStr, int* result);
int parse_int(char* inputStr, int* result);
int parse_float(char* inputStr, float* result);

#endif
