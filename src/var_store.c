#include "./types.h"
#include "./var_store.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_INTEGER 2147483647-1

struct VariableStore* var_store_init()
{
    struct VariableStore* store = (struct VariableStore*) malloc(sizeof(struct VariableStore));
    if (store == NULL) {
        printf_wrap("[ERR] VARSTORE: malloc failed for variable store \n");
        return NULL;
    }
    store->allocatedLength = VAR_STORE_INITIAL_ALLOC_LENGTH;
    store->length = 0;
    store->container = (struct VariableContainer*) malloc(sizeof(struct VariableContainer) * store->allocatedLength);
    if (store->container == NULL) {
        printf_wrap("[ERR] VARSTORE: malloc failed for first variable container \n");
        return NULL;
    }
    for (int i = 0; i < store->allocatedLength; i++) {
        store->container[i].type = 0;
    }
    var_store_add_constants(store);

    return store;
}

void var_store_add_constants(struct VariableStore* store)
{
    // NULL constant
    var_store_set(store, "NULL", TYPE_NULL, 0);
}

int var_store_hash_name(struct VariableStore* store, char* varName)
{
    int hash = 0;
    int i = 0;
    while (varName[i] != '\0') {
        int num = get_int_rep_from_char(varName[i])|integer_pow(9, i);
        hash += num;
        i++;
    }
    // FIXME: when reallocating, we should copy all variables to their new key
    // because we use modulus operator, the hash will change whenever we reallocated the table
    hash = (hash < 0 ? -hash : hash) % store->allocatedLength;
    //printf_wrap("Compute a hash for '%s' , allocatd: %d; %d\n", varName, store->allocatedLength, hash);
    return hash;
}

byte var_store_set(struct VariableStore* store, char* varName, byte type, void* valuePtr)
{
    //printf_wrap("set variable at var name: '%s' \n", varName);
    if (!(type == TYPE_FLOAT || type == TYPE_INT || type == TYPE_NULL)) {
        printf_wrap("[ERR] VARSTORE: unsupported type, cannot store type %d\n", type);
        return 0;
    }
    void* dataPtr;
    if (type == TYPE_FLOAT || type == TYPE_INT) {
        dataPtr = malloc(sizeof(int));
        if (dataPtr == NULL) {
            printf_wrap("[ERR] VARSTORE: malloc failed for data\n");
            return 1;
        }
        
        // copy the value to the place to store
        // copy from stack to heap
        memcpy(dataPtr, valuePtr, sizeof(int));
    }

    char* namePtr = (char*) malloc(strlen(varName));
    if (namePtr == NULL) {
        printf_wrap("[ERR] VARSTORE: malloc failed for var name\n");
        return 1;
    }
    strcpy(namePtr, varName);

    int originalKey = var_store_hash_name(store, varName);
    int key = originalKey;
    // handle collision, walk along the array
    while (1)
    {
        if (store->container[key].type == 0) {
            // we finally found an empty space, but we didn't found a variable with the same name
            // so we have a new variable
            store->length++;
            break;
        }
        if (strcmp(store->container[key].namePtr, varName) == 0) {
            // we found a variable with the same exact key, so we overwrite it
            break;
        }
        key = (key+1) % store->allocatedLength;
        if (key == originalKey) {
            // end the search to avoid endless loop
            printf_wrap("[ERR] VARSTORE: cannot set variable, not enough containers \n");
            return 1;
        }
    }

    //printf_wrap("set variable at key: %d \n", key);
    store->container[key].type = type;
    store->container[key].namePtr = namePtr;
    store->container[key].dataPtr = dataPtr;

    if (2*store->length >= store->allocatedLength) {
        printf_wrap("REALLOCATING THE FUCKING VAR STORE\n");
        // do smth to double the store->allocatedLength
        // e.g reallocate the store->container
        // FIXME: copy all variables to their new keys
        store->allocatedLength = 2*store->allocatedLength;
        store->container = (struct VariableContainer*) realloc(store->container, sizeof(struct VariableContainer) * store->allocatedLength);
        if (store->container == NULL) {
            printf_wrap("[ERR] VARSTORE: relloc failed for container \n");
            return 1;
        }
    }

    return 0;
}

// get the real position of a variable (in order to handle collisions)
// return -1 if no pos are found
int var_store_get_key(struct VariableStore* store, char* varName)
{
    //printf_wrap("get key for %s \n", varName);
    int originalKey = var_store_hash_name(store, varName);
    //printf_wrap("got hash for get key: %d\n", originalKey);
    int key = originalKey;
    // handle collision, walk along the array
    while (1) {
        //printf_wrap("inter key: %d %d \n", key, store->container[key].type);
        if (store->container[key].type == 0) return -1;

        // check if we found the position
        if (strcmp(store->container[key].namePtr, varName) == 0) return key;

        key = (key+1) % store->allocatedLength;
        
        if (key == originalKey) return -1;
    }
}

byte var_store_exists(struct VariableStore* store, char* varName)
{
    return var_store_get_key(store, varName) != -1;
}

byte var_store_get_type_from_key(struct VariableStore* store, int key)
{
    return store->container[key].type;
}

byte var_store_get_type(struct VariableStore* store, char* varName)
{
    int key = var_store_get_key(store, varName);
    if (key < 0) {
        return 0;
    }

    return var_store_get_type_from_key(store, key);
}

byte var_store_copy_from_key(struct VariableStore* store, int key, void* dst)
{
    if (key < 0) {
        return 1;
    }
    //printf_wrap("Var store: copy from key %d \n", key);
    memcpy(dst, store->container[key].dataPtr, (size_t) get_size_of_type(store->container[key].type));
    return 0;
}

byte var_store_copy(struct VariableStore* store, char* varName, void* dst)
{
    int key = var_store_get_key(store, varName);
    return var_store_copy_from_key(store, key, dst);
}

int var_store_get_int(struct VariableStore* store, char* varName)
{
    if (var_store_get_type(store, varName) != TYPE_INT) {
        return 0;
    }
    int val;
    var_store_copy(store, varName, &val);
    return val;
}

float var_store_get_float(struct VariableStore* store, char* varName)
{
    if (var_store_get_type(store, varName) != TYPE_FLOAT) {
        return 0;
    }
    float val;
    var_store_copy(store, varName, &val);
    return val;
}

void var_store_print(struct VariableStore* store)
{
    printf_wrap("== VarStore report (%d items) ==\n", store->length);
    for (int i = 0; i < store->allocatedLength; i++) {
        if (store->container[i].type != 0) {
            printf_wrap(
                "key: %d, '%s' = %s; ",
                i,
                store->container[i].namePtr,
                get_repr(store->container[i].type, store->container[i].dataPtr)
            );
        }
    }
    printf_wrap("== end of report\n");
}