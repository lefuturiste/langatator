#include "./utils.h"
#ifndef STACK_H_
#define STACK_H_

// struct Stack {
//     byte unitSize;
//     int length;
//     int allocated;
//     int* data;
// };

// struct Stack* stack_init();

// byte stack_push(struct Stack* stack, void* valueToPush);

// byte stack_pop(struct Stack* stack, void* valuePtr);

// byte stack_get(struct Stack* stack, int index, void* valuePtr);

// int stack_length(struct Stack* stack);

// void stack_print(struct Stack* stack);
struct IntStack {
    int length;
    int allocated;
    int* data;
};

struct IntStack* int_stack_init();

byte int_stack_push(struct IntStack* stack, int valueToPush);

byte int_stack_pop(struct IntStack* stack, int* valuePtr);

int int_stack_length(struct IntStack* stack);

void int_stack_print(struct IntStack* stack);


#endif
