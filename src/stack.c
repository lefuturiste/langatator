/*
#include "./utils.h"
#include "./stack.h"
#include <stdio.h>
#include <stdlib.h>

struct Stack* int_stack_init(byte unitSize)
{
    struct Stack* stack = (struct Stack*) malloc(sizeof(struct Stack));
    stack->unitSize = unitSize;
    stack->length = 0;
    stack->allocated = 8;
    stack->data = (int*) malloc(stack->allocated * stack->unitSize);

    return stack;
}

byte stack_push(struct Stack* stack, void* valueToPush)
{
    if (stack->length == stack->allocated) {
        stack->allocated *= 2;
        stack->data = realloc(stack->data, stack->allocated * stack->unitSize);
    }

    memcpy(stack->data[stack->length], valueToPush, stack->unitSize);
    stack->length++;

    return 1;
}

byte stack_pop(struct Stack* stack, void* valuePtr)
{
    if (stack->length == 0) {
        // error, nothing to pop
        return 0;
    }

    memcpy(valuePtr, stack->data[stack->length-1], stack->unitSize);

    stack->length--;

    return 1;
}

int int_stack_length(struct Stack* stack)
{
    return stack->length;
}

void int_stack_print(struct Stack* stack)
{
    printf_wrap("= STACK REPORT \n");
    for (int i = 0; i < stack->length; i++) {
        printf_wrap("%d; ", stack->data[i]);
    }
    printf_wrap("\n");
}


*/
#include "./utils.h"
#include "./stack.h"
#include <stdio.h>
#include <stdlib.h>

struct IntStack* int_stack_init()
{
    struct IntStack* stack = (struct IntStack*) malloc(sizeof(struct IntStack));
    stack->length = 0;
    stack->allocated = 8;
    stack->data = (int*) malloc(stack->allocated * sizeof(int));

    return stack;
}

byte int_stack_push(struct IntStack* stack, int valueToPush)
{
    if (stack->length == stack->allocated) {
        stack->allocated *= 2;
        stack->data = realloc(stack->data, stack->allocated * sizeof(int));
    }

    stack->data[stack->length] = valueToPush;
    stack->length++;

    return 1;
}

byte int_stack_pop(struct IntStack* stack, int* valuePtr)
{
    if (stack->length == 0) {
        // error, nothing to pop
        return 0;
    }

    *valuePtr = stack->data[stack->length-1];

    stack->length--;

    return 1;
}

int int_stack_length(struct IntStack* stack)
{
    return stack->length;
}

void int_stack_print(struct IntStack* stack)
{
    printf("= STACK REPORT \n");
    for (int i = 0; i < stack->length; i++) {
        printf("%d; ", stack->data[i]);
    }
    printf("\n");
}

