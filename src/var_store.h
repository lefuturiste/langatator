#include "./types.h"
#include "./utils.h"
#ifndef VAR_STORE_H_
#define VAR_STORE_H_

#define VAR_STORE_DEBUG_LEVEL (G_DEBUG_LEVEL)
#define VAR_STORE_INITIAL_ALLOC_LENGTH 64

/**

Variable store

With an Hash Table

This is a store in the heap

Features:
- resizable, when we have too much variable we can reallocate more memory to store

*/

struct VariableContainer {
    byte type;
    char* namePtr;
    void* dataPtr;
};

struct VariableStore {
    int allocatedLength;
    int length;
    struct VariableContainer* container; // with (storeAllocatedLength) items
};

struct VariableStore* var_store_init();

// add to the store some useful constants (like NULL constructor)
void var_store_add_constants(struct VariableStore* store);

int var_store_hash_name(struct VariableStore* store, char* varName);

// get a pointer to the area of memory where is store the variable
//void* var_store_get_ptr(struct VariableStore* store, char* varName);
int var_store_get_key(struct VariableStore* store, char* varName);

// return 1 if var exists 0 else
byte var_store_exists(struct VariableStore* store, char* varName);

// get type of the variable
// return NULL if the var doesn't exists
byte var_store_get_type_from_key(struct VariableStore* store, int key);
byte var_store_get_type(struct VariableStore* store, char* varName);

byte var_store_copy_from_key(struct VariableStore* store, int key, void* dest);
byte var_store_copy(struct VariableStore* store, char* varName, void* dest);

// for debug purpose
int var_store_get_int(struct VariableStore* store, char* varName);
float var_store_get_float(struct VariableStore* store, char* varName);

// OR
//byte var_store_get_ptr(char* varName, void** ptrPtr);

// store a variable
byte var_store_set(struct VariableStore* store, char* varName, byte type, void* valuePtr);

byte var_store_delete(struct VariableStore* store, char* varName);

void var_store_print(struct VariableStore* store);

#endif
