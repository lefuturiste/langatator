#include <stdio.h>
#include <string.h>
#include "./operate.h"
#include "./types.h"
#include "./utils.h"

int is_operator(char candidate) {
    return (
        candidate == '*' ||
        candidate == '/' ||
        candidate == '%' ||
        candidate == '+' ||
        candidate == '-' ||
        candidate == '^' ||
        candidate == '=' ||
        candidate == '!' ||
        candidate == '&' ||
        candidate == '|' ||
        candidate == '>' ||
        candidate == '<'
    );
}

int operate(
    byte operator,
    byte typeA, int aRepr,
    byte typeB, int bRepr,
    byte* typeRes, int* resPtr
)
{
    if (operator == '!') {
        *typeRes = TYPE_INT;
        // we will only take type B to emphezize that this unary operator is at the left of the only argument
        int x = 0;
        if (typeB == TYPE_FLOAT) x = (int) *((float*) &bRepr);
        if (typeB == TYPE_INT) x = bRepr;

        if (x == 0) *resPtr = 1;
        if (x != 0) *resPtr = 0;

        return 0;
    }
    if (operator == '=') {
        *typeRes = TYPE_INT;
        if (typeA == TYPE_FLOAT || typeB == TYPE_FLOAT) {
            *resPtr = (int) convert_to_float(typeA, aRepr) == convert_to_float(typeB, bRepr);
        }
        if (typeA == TYPE_INT && typeB == TYPE_INT) {
            *resPtr = (int) aRepr == bRepr;
        }
        
        return 0;
    }
    if (operator == '&') {
        *typeRes = TYPE_INT;
        *resPtr = (int) (convert_to_int(typeA, aRepr) & convert_to_int(typeB, bRepr));
        
        return 0;
    }
    if (operator == '|') {
        *typeRes = TYPE_INT;
        *resPtr = (int) (convert_to_int(typeA, aRepr) | convert_to_int(typeB, bRepr));
        
        return 0;
    }
    if (
        typeA == TYPE_FLOAT || typeB == TYPE_FLOAT ||
        operator == '/'
    ) {
        *typeRes = TYPE_FLOAT;
        float a, b, res;
        a = convert_to_float(typeA, aRepr);
        b = convert_to_float(typeB, bRepr);

        if (OPERATE_DEBUG_LEVEL) printf_wrap("Appling operation: %f %c %f \n", a, operator, b);
        
        if (operator == '+') {
            res = a+b;
        } else if (operator == '-') {
            res = a-b;
        } else if (operator == '*') {
            res = a*b;
        } else if (operator == '/') {
            res = a/b;
        } else if (operator == '%') {
            res = m_float_modulus(a, b);
        } else if (operator == '^') {
            res = m_float_pow(a, b);
        } else if (operator == '=') {
            res = (int) (a == b);
        } else if (operator == '>') {
            res = (int) (a > b);
        } else if (operator == '<') {
            res = (int) (a < b);
        } else {
            return 2;
        }
        if (OPERATE_DEBUG_LEVEL) printf_wrap("Got float: %f \n", res);
        // get int representation of float
        *resPtr = *(int *)(&res);
        return 0;
    }
    if (typeA == TYPE_INT && typeB == TYPE_INT) {
        if (OPERATE_DEBUG_LEVEL) printf_wrap("Appling operation %d %c %d \n", aRepr, operator, bRepr);
        *typeRes = TYPE_INT;
        int res = 0;
        if (operator == '+') {
            res = aRepr+bRepr;
        } else if (operator == '-') {
            res = aRepr-bRepr;
        } else if (operator == '*') {
            res = aRepr*bRepr;
        } else if (operator == '^') {
            res = integer_pow(aRepr, bRepr);
        } else if (operator == '%') {
            res = (aRepr % bRepr);
        } else if (operator == '=') {
            res = (int) (aRepr == bRepr);
        } else if (operator == '<') {
            res = (int) (aRepr < bRepr);
        } else if (operator == '>') {
            res = (int) (aRepr > bRepr);
        } else {
            return 2;
        }
        *resPtr = res;
        return 0;
    }

    return 3;
}
