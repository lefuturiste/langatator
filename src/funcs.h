#ifndef FUNCS_H_
#define FUNCS_H_

// // Maths functions
// #define FUNC_ABS 5

// #define FUNC_SQRT 10
// #define FUNC_EXP 11
// // natural logarithm
// #define FUNC_LN 12
// // decimal log
// #define FUNC_LOG 13
// #define FUNC_POW 14

// #define FUNC_SIN 64
// #define FUNC_COS 65
// #define FUNC_TAN 66

// #define FUNC_ACOS 72
// #define FUNC_ASIN 73
// #define FUNC_ATAN 74
// // x,y as arguments
// #define FUNC_ATAN2 75

// #define FUNC_COSH 91
// #define FUNC_SINH 92
// #define FUNC_TANH 93

// // factorial
// #define FUNC_FACT 101
// // choose (n,k) (binomial)
// #define FUNC_CHOOSE 102

// // Greatest common divisor
// #define GCD 150
// // Least common multiple
// #define LCM 151

// // Input/Output Functions
// #define FUNC_PRINT 201
// #define FUNC_INPUT 202

// will return the function id from a string or 0 if not found

short identify_func_name(char* candidate);

int execute_func(short funcID, short argsLen, unsigned char* argsTypes, int* argsValues, int* resPtr, unsigned char* resTypePtr);

#endif
