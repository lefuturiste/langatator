#include "./utils.h"
#ifndef TYPES_H_
#define TYPES_H_

#define TYPE_NULL 1
#define TYPE_INT 2
#define TYPE_FLOAT 3
#define TYPE_OPERATOR 32
#define TYPE_COMMA 33
#define TYPE_FUNC_NAME 34
#define TYPE_VAR_NAME 35
#define TYPE_OPEN_PARENTHESIS 64
#define TYPE_CLOSE_PARENTHESIS 65


int is_type_literal(byte type);

int is_type_number(byte type);

int get_size_of_type(byte type);

// get short string representation of a type
// usefull when printing a report
char* get_repr(byte type, void* data);

byte convert_to_bool(byte type, int repr);
int convert_to_int(byte type, int repr);
float convert_to_float(byte type, int repr);

#endif
