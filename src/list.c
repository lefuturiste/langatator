#include "./types.h"
#include "./list.h"
#include <stdio.h>
/*

struct List {
    int num_elements;
    int bytes_used;
    int data_ptr; // this is the pointer where there is freespace
    int ptrArray[LIST_SIZE];
    unsigned char typeArray[LIST_SIZE];
    unsigned char data[LIST_SIZE];
};
*/

unsigned char list_get_type(struct List* list, int index) {
    if (index >= list->num_elements) {
        return 0;
    }

    return list->typeArray[index];
}

int list_get(struct List* list, int index, void* resultPtr)
{
    if (index >= list->num_elements) {
        // this element does not exist for now
        return 1;
    }
    unsigned char type = list->typeArray[index];
    if (type == 0) {
        // we skip this element
        return 1;
    }

    int size = get_size_of_type(type);
    int addr = list->ptrArray[index];
    
    int baseVal = 0;
    unsigned char* valuePtr = (unsigned char*) &baseVal;

    for (int i = 0; i < size; i++) {
        valuePtr[i] = list->data[addr+i];
    }
    
    // convert to underlying C types
    if (type == TYPE_INT) {
        *((int*) resultPtr) = *((int*) valuePtr);
    }
    if (type == TYPE_FLOAT) {
        *((float*) resultPtr) = *((float*) valuePtr);
    }
    if (type == TYPE_OPERATOR) {
        *((char*) resultPtr) = *((char*) valuePtr);
    }
    if (type == TYPE_FUNC_NAME || type == TYPE_VAR_NAME) {
        *((short*) resultPtr) = *((short*) valuePtr);
    }

    return 0;
}

int list_get_int(struct List* list, int index, int* valuePtr)
{
    int type = list_get_type(list, index);
    if (!type) {
        return 1;
    }
    if (type != TYPE_INT) {
        // invalid type
        return 2;
    }
    return list_get(list, index, (void*) valuePtr);
}

int list_get_float(struct List* list, int index, float* valuePtr)
{
    int type = list_get_type(list, index);
    if (!type) {
        return 1;
    }
    if (type != TYPE_FLOAT) {
        // invalid type
        return 2;
    }
    return list_get(list, index, (void*) valuePtr);
}

int list_set(struct List* list, int index, unsigned char type, void* valuePtr)
{
    if (index >= list->num_elements) {
        // this is a new element
        if (list->bytes_used == LIST_SIZE) {
            // can't add, we are already complete
            return 1;
        }
        list->num_elements++;
    }

    // for now, we just add at the end of the data array
    int size = get_size_of_type(type);
    if (size < 0) {
        // the type is invalid
        return 2;
    }

    unsigned char* data = (unsigned char*) valuePtr;
    int start = list->data_ptr;
    for (int i = 0; i < size; i++) {
        //list->data[start+i] = value & (255<<i);
        list->data[start+i] = data[i];
    }

    list->ptrArray[index] = start;
    list->typeArray[index] = type;
    list->data_ptr += size;

    return 0;
}


int list_set_int(struct List* list, int index, int value)
{
    return list_set(list, index, TYPE_INT, &value);
}

int list_set_float(struct List* list, int index, float value)
{
    return list_set(list, index, TYPE_FLOAT, &value);
}

int list_set_char(struct List* list, int index, char value)
{
    return list_set(list, index, TYPE_OPERATOR, &value);
}

void arr_delete_by_gravity(void* src, int elementLength, int length, int index)
{
    unsigned char* srcPtr = (unsigned char*) src;
    for (int i = index; i < length; i++) {
        if (i == length-1) {
            for (int j = 0; j < elementLength; j++) {
                srcPtr[i*elementLength+j] = 0;
            }
            continue;
        }
        for (int j = 0; j < elementLength; j++) {
            srcPtr[i*elementLength+j] = srcPtr[((i+1)*elementLength)+j];
        }
    }
}

int list_delete(struct List* list, int index)
{
    if (index >= list->num_elements) {
        // error: index out of range
        return 1;
    }
    arr_delete_by_gravity(&list->typeArray, 1, list->num_elements, index);
    arr_delete_by_gravity(&list->ptrArray, 4, list->num_elements, index);
    list->num_elements--;

    return 0;
}

void list_reset(struct List* list)
{
    for (int i = 0; i < list->num_elements; i++) {
        list_delete(list, 0);
    }
    list->data_ptr = 0;
    list->bytes_used = 0;
    list->num_elements = 0;
}

int list_append_int(struct List* list, int value)
{
    return list_set_int(list, list->num_elements, value);
}

int list_append_char(struct List* list, char value)
{
    return list_set_char(list, list->num_elements, value);
}

void list_print(struct List* list)
{
    printf_wrap("=== LIST REPORT === \n");
    printf_wrap("num of elements: %d \n", list->num_elements);
    for (int i = 0; i < list->num_elements; i++) {
        
        int type = list_get_type(list, i);
        int data = 0;
        list_get(list, i, &data);
        
        printf_wrap("- i: %d, %s \n", i, get_repr(type, (void*) &data));
    }
    printf_wrap("=== END === \n");
}
