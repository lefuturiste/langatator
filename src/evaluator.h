#include "./state.h"
#ifndef EVALUATOR_H_
#define EVALUATOR_H_

#define EVALUATOR_DEBUG_LEVEL (G_DEBUG_LEVEL)

int evaluate(struct StateContainer* state, char* inputStr, int* resultPtr, unsigned char* typePtr);

#endif
