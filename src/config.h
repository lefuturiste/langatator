#include "./config_env.h"

//#define printf_wrap(template,...) printf_wrap(template, ##__VA_ARGS__);
#define getline_wrap(...) getline(__VA_ARGS__);

#ifndef G_CONFIG_H_
#define G_CONFIG_H_

#define PRINT_METHOD_CLASSIC 0
#define PRINT_METHOD_WASM 1
// #define CONFIG_PRINT_METHOD 

#define PRESET_ENGLISH 0
#define PRESET_FRENCH 1

// 0: no logs; 1: medium debug; 2: full debug
#define G_DEBUG_LEVEL 2
#define SYNTAX_PRESET PRESET_ENGLISH

// Define your own custom syntax here

#if SYNTAX_PRESET == PRESET_ENGLISH
    #define SYNTAX_END "end"
    #define SYNTAX_EXIT "exit"

    #define SYNTAX_SET_START "set"
    #define SYNTAX_SET_STOP "to"

    #define SYNTAX_IF_START "if"
    #define SYNTAX_IF_STOP "then"

    #define SYNTAX_WHILE_START "while"
    #define SYNTAX_WHILE_STOP "do"

    #define SYNTAX_CONTINUE "continue"
    #define SYNTAX_BREAK "break"
#endif

#if SYNTAX_PRESET == PRESET_FRENCH
    #define SYNTAX_END "fin"
    #define SYNTAX_EXIT "sortir"

    #define SYNTAX_SET_START "definir"
    #define SYNTAX_SET_STOP "comme"

    #define SYNTAX_IF_START "si"
    #define SYNTAX_IF_STOP "alors"

    #define SYNTAX_WHILE_START "tant que"
    #define SYNTAX_WHILE_STOP "faire"

    #define SYNTAX_CONTINUE "continuer"
    // JPP
    #define SYNTAX_BREAK "casser"
#endif

#endif