#include "./utils.h"
#include "./types.h"
#include <stdio.h>
#include <stdlib.h>

int is_type_literal(byte type)
{
    return is_type_number(type) || type == TYPE_NULL;
}

int is_type_number(byte type)
{
    return type == TYPE_INT || type == TYPE_FLOAT;
}

// get size in byte of a particular type
int get_size_of_type(byte type)
{
    int t = (int) type;
    // underlying type is int (4 bytes)
    if (t == TYPE_INT || t == TYPE_FLOAT) {
        return 4;
    }
    // underlying type is short (2 bytes)
    if (t == TYPE_FUNC_NAME || t == TYPE_VAR_NAME) {
        return 2;
    }
    // underlying type is char (1 byte)
    if (t == TYPE_OPERATOR) {
        return 1;
    }
    if (
        t == TYPE_NULL ||
        t == TYPE_OPEN_PARENTHESIS ||
        t == TYPE_CLOSE_PARENTHESIS ||
        t == TYPE_COMMA
    ) {
        return 0;
    }
    return -1;
}

char* get_repr(byte type, void* valPtr)
{
    char* res = (char*) malloc(sizeof(char)*32);
    if (res == 0) return 0;
    if (type == 0) {
        sprintf(res, "NO_TYPE");
    } else if (type == TYPE_NULL) {
        sprintf(res, "NULL");
    } else if (type == TYPE_INT) {
        sprintf(res, "INT(%d)", *((int*) valPtr));
    } else if (type == TYPE_FLOAT) {
        sprintf(res, "FLOAT(%f)", *((float*) valPtr));
    } else if (type == TYPE_OPEN_PARENTHESIS) {
        sprintf(res, "OPEN_PARENTHESIS");
    } else if (type == TYPE_CLOSE_PARENTHESIS) {
        sprintf(res, "CLOSE_PARENTHESIS");
    } else if (type == TYPE_COMMA) {
        sprintf(res, "COMMA");
    } else if (type == TYPE_OPERATOR) {
        sprintf(res, "OPERATOR(%c)", *((char*) valPtr));
    } else if (type == TYPE_VAR_NAME) {
        sprintf(res, "VAR_NAME(ref: %d)", *((int*) valPtr));
    } else if (type == TYPE_FUNC_NAME) {
        sprintf(res, "FUNC_NAME(ref: %d)", *((int*) valPtr));
    } else {
        sprintf(res, "UNKNOWN type=%d", type);
    }
    return res;
}

int convert_to_int(byte type, int repr) {
    if (type == TYPE_NULL) return 0;
    if (type == TYPE_FLOAT) return (int) *((int*) &repr);
    return repr;
}

byte convert_to_bool(byte type, int repr) {
    return (byte) (convert_to_int(type, repr) == 1);
}

float convert_to_float(byte type, int repr) {
    if (type == TYPE_INT) return (float) repr;
    return *((float*) &repr);
}
