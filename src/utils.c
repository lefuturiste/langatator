#include <stdio.h>
#include <string.h>
#include "./utils.h"

#include <stdarg.h>

#if PRINT_MODE == 0
int printf_wrap(const char* format, ...) {
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end( args );
}
#endif

int get_int_rep_from_float(float ft)
{
    return *(int *)(&ft);
}

/**
 * Get a single precision floating point from int representation
 * (following IEEE 754)
 */
float get_float_from_int_rep(int representation)
{
    float res;
    *((int*) &res) = representation;

    return res;
}

int get_int_rep_from_char(char c)
{
    return (int) c;
}

int is_char_numeral(char candidate)
{
    int n = (int) candidate;
    return (n >= 48 && n <= 57);
}

int float_almost_equal(float a, float b)
{
    float diff = a-b;
    if (diff < 0) diff = -diff;

    return diff < 0.001;
}

int abs_int(int a)
{
    return a < 0 ? -a : a;
}

float abs_float(float a)
{
    return a < 0 ? -a : a;
}

int integer_pow(int base, int exponent)
{
    if (exponent == 0) return 1;
    int r = 1;
    int multiplier = base;
    while (exponent) {
        // if exponent is even
        if (exponent & 1) {
            r *= multiplier;
        }

        multiplier *= multiplier;
        exponent >>= 1; // divide by two the exponent
    }

    return r;
}

float m_float_pow(float base, int exponent)
{
    if (exponent == 0) return 1;
    float r = 1;
    float multiplier = base;
    while (exponent) {
        // if exponent is even
        if (exponent & 1) {
            r *= multiplier;
        }

        multiplier *= multiplier;
        exponent >>= 1; // divide by two the exponent
    }

    return r;
}

float m_float_modulus(float a, float b)
{
    // find the remainder
    float t = a;
    int i = 0;
    while (i < 1000) {
        t = t - b;
        if (t < 0) {
            return t+b;
        }
        i++;
    }
    return -1;
}

int m_factorial(int x)
{
    if (x == 0 || x == 1) {
        return 1;
    }
    int res = 1;
    for (int i = 2; i < x+1; i++) {
        res *= i;
    }
    return res;
}

float m_exp(float x)
{
    // TODO: reduce the x to be more accurate in the exp computation
    const int n = 13;
    float out = 0;
    for (int i = 0; i < n; i++) {
        out += m_float_pow(x, i)/m_factorial(i);
    }
    return out;
}

float m_float_mod(float a, float b)
{
    float mod;
    // Handling negative values
    if (a < 0)
        mod = -a;
    else
        mod =  a;
    if (b < 0)
        b = -b;
 
    // Finding mod by repeated subtraction
     
    while (mod >= b)
        mod = mod - b;
 
    // Sign of result typically depends
    // on sign of a.
    if (a < 0)
        return -mod;
 
    return mod;
}

float m_sin(float originalX)
{
    // use cycles in expension series
    if (originalX > 2*CST_PI) {
        originalX = m_float_mod(originalX, (float) (2*CST_PI));
    }
    float x = originalX;

    if (originalX > CST_PI) {
        x -= CST_PI;
    }

    const int n = 6;
    float out = 0;

    int sign = 1;
    for (int i = 0; i < n; i++) {
        out += sign*(m_float_pow(x, 2*i+1)/m_factorial(2*i+1));
        
        if (sign == 1) {
            sign = -1;
        } else {
            sign = 1;
        }
    }

    if (originalX > CST_PI) {
        return -out;
    }
    return out;
}

float m_cos(float x)
{
    return m_sin(x+CST_PI/2);
}

float m_tan(float x)
{
    return m_sin(x)/m_cos(x);
}

// float m_get_derivative(float (*func)(float), float a)
// {
//     float h = 0.0001;
//     return ((func(a+h)-func(a))/h);
// }

// newton method in one dimension
// takes a function and find one of the root
// return the status 0 if success >0 if error
int m_newton_method(float (*func)(float, float), float param, float startsAt, float* resPtr)
{
    // (x-b)f'(b)+f(b) = 0
    // xf'(b)-bf'(b)+f(b) = 0
    // x = (-f(b)+bf'(b))/f'(b)

    float cursor = startsAt;
    float newCursor = 0;
    float derivative = 0;
    short runs = 0;
    const float h = 0.0001;
    while (1)
    {
        derivative = (func(param, cursor+h)-func(param, cursor))/h;
        newCursor = cursor - func(param, cursor)/derivative;
        if (abs_float(cursor - newCursor) < 0.0001) {
            *resPtr = (cursor + newCursor)/2;
            return 0;
        }
        if (runs > 100) {
            printf_wrap("ERR: newton methods failed, coup dur pour newton \n");
            return 100;
        }
        cursor = newCursor;
        runs++;
    }

    return 101;
}

float m_sqrt_equation(float x, float y)
{
    return m_float_pow(y,2)-x;
}

float m_sqrt(float x)
{
    float res = 0;
    m_newton_method(&m_sqrt_equation, x, 4, &res);

    return res;
}

float m_ln_equation(float x, float y)
{
    return m_exp(y)-x;
}

float m_ln(float x)
{
    float res = 0;
    m_newton_method(&m_ln_equation, x, 4, &res);

    return res;
}

float m_log(float base, float x)
{
    // b^x = y
    // log_b(b^x) = log_b(y)
    // x = log_b(y)

    // exp(x ln(b)) = y
    // x ln(b) = ln(y)
    // x = ln(y)/ln(b)

    return m_ln(x)/m_ln(base);
}

int is_full_of_space(char* str)
{
    int i = 0;
    while (str[i] != '\0') {
        if (str[i] != ' ') {
            return 0;
        }
        i++;
    }
    return 1;
}

void trim_space(char* dst, char* subject)
{
    // trim spaces on both sides
    int start = 0;
    int end = strlen(subject)-1;
    while (subject[start] == ' ') start++;
    while (subject[end] == ' ') end--;
    end += 1;

    str_extract(dst, subject, start, end-start);
}

// FIXME: not protected against memory overflow
void str_extract(char* dst, char* subject, int initial, int length)
{
    for (int i = 0; i < length; i++) {
        dst[i] = subject[initial+i];
    }
    dst[length] = '\0';
}

byte str_needle_at_pos(char* needle, char* subject, int atPos) {
    size_t needleLen = strlen(needle);
    if (strlen(subject) < needleLen) {
        return 0;
    }
    char ahead[strlen(needle)+1];
    str_extract((char*) &ahead, subject, atPos, needleLen);
    return strcmp(needle, ahead) == 0;
}

byte str_starts_with(char* needle, char* subject) {
    return str_needle_at_pos(needle, subject, 0);
}

// FIXME: dead code
int cmp_str_in_place(char* against, char* str, int from)
{

    int i = 0;
    while (against[i] != '\0') {
        if (against[i] != str[from+i]) {
            return 0;
        }
        i++;
    }
    return 1;
}
