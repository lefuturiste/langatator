#include "./state.h"
#ifndef LINE_PROCESSING_H_
#define LINE_PROCESSING_H_

#define LINE_PROCESSING_DEBUG_LEVEL (G_DEBUG_LEVEL)

#define BLOCK_IF 2
#define BLOCK_WHILE 3

int recognize_word(char* lineStr, char* word);
int recognize_termination_keyword(char* needle, char* subject, int from);

struct SetStatement {
    short name_start;
    short name_stop;
    short expression_start;
};
int recognize_set_statement(char* lineStr, struct SetStatement* res);

struct IfStatement {
    short expression_start;
    short expression_stop;
};
int recognize_if_statement(char* lineStr, struct IfStatement* res);

struct WhileStatement {
    short expression_start;
    short expression_stop;
};
int recognize_while_statement(char* lineStr, struct WhileStatement* res);

int process_script(struct StateContainer* state, char* script);

int process_line(struct StateContainer* state, char* str);

#endif
