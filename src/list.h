#ifndef LIST_H_
#define LIST_H_

/*
Dynamic lists
We want a data structure which is fast to read from
But not necessarily fast to write to
So we need to have a map of the data structure, so we know where to look

Since each list element can be of different size, we need to reference the address of each element

So the first thing we need is two arrays of int that we serve us as metadata arrays:
- one for the types of the elements (call it the typeArray)
- one for the addresses of the elements (call it the ptrArray)
these two arrays must remain in sync

Then we have the third array which we will call the dataArray in which we actually store the data

When we neeed to read an element from it's index, it's simple:
1. first we get the type of the element by it's index, if it's null we know that this element doesn't exists
2. second we get the start address of the element and we retrieve the bytes to reconstruct the right data type

On the other side, when we need to write an element it's a little bit more complicated:
- We know which data type we want to add, so we know what size we are looking for in the main
- We will swipe through all the data array in a desperate search for a place where we can fit our data. If we reached the end without find any place, we can either give up or launch a more complicated to move some bytes around (reorganization) in a process called defragmentation.
- If we found a place, we add the pointer to the start of that region in memory in the ptrArray and set the typeArray at the right index
- But we will have to move all the elements in the ptrArray and typeArray by gravity
- We update num_elements and bytes_used in the struct

- a simpler startegry to add data, is to jump directly at the end where we get clean space and put our data here, at the expense of loosing memory with garbage in the memory.

The minimum size of an element is 8 bits - 1 byte (for unsigned char)
So if the size of the list is N bytes, we can have a maximum of N single elements
And the maximum size of an element is 32 bytes - 4 bytes (for normal int)

*/

// how many bytes we alocate for the list
#define LIST_SIZE 4096

struct List {
    int num_elements;
    int bytes_used; // this is the pointer where there is freespace
    int data_ptr;
    int ptrArray[LIST_SIZE];
    unsigned char typeArray[LIST_SIZE];
    unsigned char data[LIST_SIZE]; // FIXME: remove this bc we want to use malloc
};

unsigned char list_get_type(struct List* list, int index);

int list_get(struct List* list, int index, void* valuePtr);
int list_get_int(struct List* list, int index, int* valuePtr);
int list_get_char(struct List* list, int index, char* valuePtr);

int list_set(struct List* list, int index, unsigned char type, void* valuePtr);
int list_set_int(struct List* list, int index, int value);
int list_set_char(struct List* list, int index, char value);

int list_append_int(struct List* list, int value);
int list_append_char(struct List* list, char value);

int list_delete(struct List* list, int index);

void list_print(struct List*);

void list_reset(struct List* list);

#endif
