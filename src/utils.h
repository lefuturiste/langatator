#include "./config.h"

#ifndef UTILS_H_
#define UTILS_H_

int printf_wrap(const char* format, ...);

// useful to silent warning of gcc of uunused parameters in functions
#define UNUSED(x) (void)(x)

#define CST_PI 3.1415926535

// define custom type (unsigned char)
typedef unsigned char byte;


int get_int_rep_from_float(float ft);

float get_float_from_int_rep(int representation);

int get_int_rep_from_char(char c);

int is_char_numeral(char candidate);

int integer_pow(int base, int exponent);

int float_almost_equal(float a, float b);

float m_float_modulus(float a, float mod);

int m_factorial(int x);


int m_newton_method(float (*func)(float, float), float param, float startsAt, float* resPtr);

float m_float_pow(float base, int exponent);

float m_sqrt(float x);

float m_exp(float x);

float m_ln(float x);

float m_log(float base, float x);

float m_sin(float x);

float m_cos(float x);

float m_tan(float x);

int is_full_of_space(char* str);

void str_extract(char* res, char* subject, int initial, int length);

byte str_needle_at_pos(char* needle, char* subject, int pos);

byte str_starts_with(char* needle, char* subject);

void trim_space(char* dst, char* subject);

#endif
