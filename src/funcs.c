#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include "./config.h"
#include "./types.h"
#include "./utils.h"
#include "./number_parsing.h"

// just return the type id of a variable
int type_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    UNUSED(args);
    *resType = TYPE_INT;
    *res = types[0];

    return 0;
}

int is_null_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    UNUSED(args);
    *resType = TYPE_INT;
    *res = types[0] == TYPE_NULL;

    return 0;
}

int is_number_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    UNUSED(args);
    *resType = TYPE_INT;
    *res = is_type_number(types[0]);

    return 0;
}

int abs_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    if (types[0] == TYPE_INT) {
        *res = args[0];
        if (args[0] < 0) {
            *res = -args[0];
        }
        *resType = TYPE_INT;
        return 0;
    }
    if (types[0] == TYPE_FLOAT) {
        float val = get_float_from_int_rep(args[0]);
        if (val < 0) {
            val = -val;
        }
        *res = get_int_rep_from_float(val);
        *resType = TYPE_FLOAT;
        return 0;
    }
    return 1;
}

int floor_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    *resType = TYPE_INT;
    if (types[0] == TYPE_INT) {
        *res = args[0];
        return 0;
    }
    if (types[0] == TYPE_FLOAT) {
        float val = get_float_from_int_rep(args[0]);
        
        *res = (int) val;
        return 0;
    }
    return 1;
}

int ceil_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    int resInterm = 0;
    if (floor_impl(&resInterm, resType, types, args)) {
        return 1;
    }
    resInterm += 1;
    *res = resInterm;
    return 0;
}

int print_number_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    *resType = TYPE_INT;
    if (!is_type_number(types[0])) {
        *res = 0;
        return 1;
    }
    if (G_DEBUG_LEVEL >= 1) {
        printf_wrap("REAL_PRINT: ");
    }
    if (types[0] == TYPE_INT) {
        int val = args[0];
        printf_wrap("%d\n", val);
    }
    if (types[0] == TYPE_FLOAT) {
        float val = get_float_from_int_rep(args[0]);
        printf_wrap("%f\n", val);
    }
    *res = 1;
    return 0;
}

int print_ascii_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    *resType = TYPE_INT;
    if (!is_type_number(types[0])) {
        *res = 0;
        return 1;
    }
    
    int charVal = 0;
    if (types[0] == TYPE_INT) {
        charVal = args[0];
    }
    if (types[0] == TYPE_FLOAT) {
        charVal = (int) get_float_from_int_rep(args[0]);
    }

    printf_wrap("%c", *((char*) &charVal));
    *res = 1;
    return 0;
}

int print_newline_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    UNUSED(types);
    UNUSED(args);

    *resType = TYPE_INT;
    *res = 1;
    printf_wrap("\n");

    return 0;
}

int input_number_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    UNUSED(types);
    UNUSED(args);
    printf_wrap("? ");

    char* line;
    size_t len = 0;
    size_t lineSize = 0;

    lineSize = getline_wrap(&line, &len, stdin);

    // printf_wrap("len=%d, lineSize=%d '%s' \n", len, lineSize, line);

    char toParse[lineSize+1];
    str_extract((char*) toParse, line, 0, lineSize-1);

    int st = parse_int((char*) &toParse, res);
    if (st == 0) {
        *resType = TYPE_INT;
        return 0;
    }
    if (st != 0) {
        st = parse_float((char*) &toParse, (float*) res);
        if (st == 0) {
            *resType = TYPE_FLOAT;
            return 0;
        }
    }
    // we didn't manage to parse the number
    *resType = TYPE_INT;
    *res = 0;
    
    return 0;
}

int simple_float_func(float (*func)(float), int* res, unsigned char* resType, unsigned char* types, int* args)
{
    float x = 0;
    if (types[0] == TYPE_INT) {
        x = (float) args[0];
    }
    if (types[0] == TYPE_FLOAT) {
        x = get_float_from_int_rep(args[0]);
    }
    *res = get_int_rep_from_float(func(x));
    *resType = TYPE_FLOAT;

    return 0;
}

/*
 #define SIMPLE_FUNC_BINDING(name) ({\
     int #name_impl(int* res, unsigned char* resType, unsigned char* types, int* args)\
     {\
         return simple_float_func(&m_#name, res, resType, types, args);\
     }\
 })
*/

// SIMPLE_FUNC_BINDING(sqrt)
// 
// SIMPLE_FUNC_BINDING(exp)
// SIMPLE_FUNC_BINDING(ln)
// SIMPLE_FUNC_BINDING(cos)
// SIMPLE_FUNC_BINDING(sin)
// SIMPLE_FUNC_BINDING(tan)

int sqrt_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    return simple_float_func(&m_sqrt, res, resType, types, args);
}

int exp_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    return simple_float_func(&m_exp, res, resType, types, args);
}

int cos_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    return simple_float_func(&m_cos, res, resType, types, args);
}

int sin_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    return simple_float_func(&m_sin, res, resType, types, args);
}

int tan_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    return simple_float_func(&m_tan, res, resType, types, args);
}

int ln_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    return simple_float_func(&m_ln, res, resType, types, args);
}

int log_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    float x = 0;
    if (types[0] == TYPE_INT) {
        x = (float) args[0];
    }
    if (types[0] == TYPE_FLOAT) {
        x = get_float_from_int_rep(args[0]);
    }
    *res = get_int_rep_from_float(m_log(10, x));
    *resType = TYPE_FLOAT;

    return 0;
}

int max_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    short maxIndex = 0;
    
    float a = 0;
    float b = 0;
    if (types[0] == TYPE_FLOAT) {
        a = get_float_from_int_rep(args[0]);
    }
    if (types[1] == TYPE_FLOAT) {
        b = get_float_from_int_rep(args[1]);
    }
    if (types[0] == TYPE_INT) {
        a = (float) args[0];
    }
    if (types[1] == TYPE_INT) {
        b = (float) args[1];
    }
    if (b > a) {
        maxIndex = 1;
    }
    
    *res = args[maxIndex];
    *resType = types[maxIndex];

    return 0;
}

int get_pi_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    UNUSED(args);
    UNUSED(types);
    float val = CST_PI;
    *res = *(int *)(&val);
    *resType = TYPE_FLOAT;
    return 0;
}

/**
random_int(a, b)
Return a random int between a and b (a and b included)
*/
int random_int_impl(int* res, unsigned char* resType, unsigned char* types, int* args)
{
    srand(time(NULL));

    int lower = 1;
    int upper = 10;
    if (types[0] == TYPE_FLOAT) {
        lower = (int) get_float_from_int_rep(args[0]);
    }
    if (types[1] == TYPE_FLOAT) {
        upper = (int) get_float_from_int_rep(args[1]);
    }
    if (types[0] == TYPE_INT) {
        lower = args[0];
    }
    if (types[1] == TYPE_INT) {
        upper = args[1];
    }
    
    // rand returns a value between 0 and RAND_MAX (which is close to infinite)
    int num = (rand() % (upper - lower + 1)) + lower;
    *resType = TYPE_INT;
    *res = num;

    return 0;
}

struct FuncIntro {
    char name[20];
    int (*implementation)(int*, unsigned char*, unsigned char*, int*);
    short nbArgs;
};

// void* are actually long!

struct FuncIntro intros[] = {
    {"type", &type_impl, 1},
    {"is_null", &is_null_impl, 1},
    {"is_number", &is_number_impl, 1},

    {"abs", &abs_impl, 1},
    
    {"sqrt", &sqrt_impl, 1},
    {"exp", &exp_impl, 1},
    
    {"sin", &sin_impl, 1},
    {"cos", &cos_impl, 1},
    {"tan", &tan_impl, 1},
    {"ln", &ln_impl, 1},
    {"log", &log_impl, 1},

    {"random_int", &random_int_impl, 2},

    {"max", &max_impl, 2},
    {"floor", &floor_impl, 1},
    {"ceil", &ceil_impl, 1},
    {"get_pi", &get_pi_impl, 0},

    {"print_number", &print_number_impl, 1},
    {"print_ascii", &print_ascii_impl, 1},
    {"print_newline", &print_newline_impl, 0},
    {"input_number", &input_number_impl, 0},
    {"", 0, 0}
};

static int nbOfFuncs = sizeof(intros)/sizeof(struct FuncIntro);

/**
Will return the function id
*/
short identify_func_name(char* candidate)
{
    for (int i = 0; i < nbOfFuncs; i++) {
        if (strcmp(intros[i].name, candidate) == 0) {
            return i;
        }
    }
    return -1;
}

int execute_func(short funcID, short argsLen, unsigned char* argsTypes, int* argsValues, int* resPtr, unsigned char* resTypePtr)
{
    if (funcID >= nbOfFuncs) {
        printf_wrap("ERR: Invalid func with index: %d \n", funcID);
        return 1;
    }
    int (*impl)(int*, unsigned char*, unsigned char*, int*) = intros[funcID].implementation;
    if (impl == 0) {
        printf_wrap("ERR: No implementation for func with index: %d \n", funcID);
        return 1;
    }
    char* name = intros[funcID].name;
    if (G_DEBUG_LEVEL >= 2) printf_wrap("Executing func '%s' \n", name);
    if (argsLen < intros[funcID].nbArgs) {
        printf_wrap("ERR: Too few arguments for func call '%s' \n", name);
        return 1;
    }
    if (argsLen > intros[funcID].nbArgs) {
        printf_wrap("ERR: Too many arguments for func call '%s' \n", name);
        return 1;
    }

    // call the function implementation
    // first cast the function ptr
    impl(resPtr, resTypePtr, argsTypes, argsValues);

    if (G_DEBUG_LEVEL >= 2) printf_wrap("Got %s \n", get_repr(*resTypePtr, resPtr));

    return 0;
}
