#include <stdio.h>
#include <string.h>
#include "./number_parsing.h"
#include "./utils.h"

int parse_clean_positive_integer(int inputStrLen, char* inputStr, int* result) {
    /**
    Parse an integer that is just a continus stream of decimal numbers
    without any negative sign
    **/
    int i = 0;
    int value = 0;
    
    while (inputStr[i] != 0) {
        if (!is_char_numeral(inputStr[i])) {
            return 1;
        }
        int numeralIndex = (int) inputStr[i];
        value += (
            integer_pow(10, inputStrLen-(i+1)) *
            (numeralIndex & 0b001111)
        );
        i++;
    }
    //printf_wrap("parseclean %d \n", value);

    *result = value;

    return 0;
}

int parse_int(char* inputStr, int* resultPtr) {
    int i = 0;
    char cleanStr[strlen(inputStr)+1];
    int cleanStrLen = 0;
    int hasNegSign = 0;
    while (inputStr[i] != 0) {
        if (
            !is_char_numeral(inputStr[i]) &&
            inputStr[i] != '-' &&
            inputStr[i] != ' ' &&
            inputStr[i] != '_'
        ) {
            // we've encounter a unexpected char
            return 1;
        }
        if (inputStr[i] == '-' && (hasNegSign || i > 0)) {
            // we'v encounter a sign but we already have one or it's too far
            return 1;
        }
        if (!hasNegSign && inputStr[i] == '-') {
            hasNegSign = 1;
        }
        if (is_char_numeral(inputStr[i])) {
            cleanStr[cleanStrLen] = inputStr[i];
            cleanStrLen++;
        }
        i++;
    }
    cleanStr[cleanStrLen] = 0;
    //printf_wrap("clean str: %s/ \n", cleanStr);

    int inter = 0;
    byte stat = parse_clean_positive_integer(cleanStrLen, cleanStr, &inter);
    if (NB_PARSING_DEBUG_LEVEL >= 2) {
        printf_wrap("Parse clean positive integer stat: %d\n", stat);
        printf_wrap("Got value: %d\n", inter);
    }

    *resultPtr = inter;
    if (hasNegSign) {
        *resultPtr = -1 * inter;
    }
    
    return 0;
}

int parse_float(char* inputStr, float* resultPtr) {
    if (NB_PARSING_DEBUG_LEVEL >= 2) printf_wrap("Parsing as float: '%s'\n", inputStr);
    int i = 0;
    char cleanStrIntPart[strlen(inputStr)+1];
    int cleanStrIntPartLen = 0;
    
    char cleanStrFloatPart[strlen(inputStr)+1];
    int cleanStrFloatPartLen = 0;

    int part = 0; // 0 for first part, 1 for snd part

    int hasNegSign = 0;

    while (inputStr[i] != 0) {
        if (inputStr[i] == '.') {
            // we switch part
            part = 1;
            i++;
            continue;
        }
        if (part == 0) {
            if (
                !is_char_numeral(inputStr[i]) &&
                inputStr[i] != '-' &&
                inputStr[i] != ' ' &&
                inputStr[i] != '_'
            ) {
                // we've encounter a unexpected char
                return 10;
            }
            if (inputStr[i] == '-' && (hasNegSign || i > 0)) {
                // we'v encounter a sign but we already have one or it's too far
                return 20;
            }
            if (!hasNegSign && inputStr[i] == '-') {
                hasNegSign = 1;
            }
            if (is_char_numeral(inputStr[i])) {
                cleanStrIntPart[cleanStrIntPartLen] = inputStr[i];
                cleanStrIntPartLen++;
            }
        }
        if (part == 1) {
            if (
                !is_char_numeral(inputStr[i]) &&
                inputStr[i] != ' ' &&
                inputStr[i] != '_'
            ) {
                // we've encounter a unexpected char
                return 30;
            }
            if (is_char_numeral(inputStr[i])) {
                cleanStrFloatPart[cleanStrFloatPartLen] = inputStr[i];
                cleanStrFloatPartLen++;
            }
        }
        i++;
    }
    cleanStrIntPart[cleanStrIntPartLen] = 0;
    cleanStrFloatPart[cleanStrFloatPartLen] = 0;
    // printf_wrap("clean str int part: /%s/ \n", cleanStrIntPart);
    // printf_wrap("clean str float part: /%s/ \n", cleanStrFloatPart);

    int intPart = 0;
    parse_clean_positive_integer(cleanStrIntPartLen, cleanStrIntPart, &intPart);

    int floatPart = 0;
    parse_clean_positive_integer(cleanStrFloatPartLen, cleanStrFloatPart, &floatPart);

    float tmp1 = ((float)floatPart / ((float) integer_pow(10, cleanStrFloatPartLen)));
    float inter = ((float) intPart) + tmp1;

    *resultPtr = inter;
    if (hasNegSign) {
        *resultPtr = -1 * inter;
    }
    
    return 0;
}
