#include "./config.h"
#include "./types.h"
#include "./utils.h"
#include "./line_processing.h"
#include "./stack.h"
#include "./evaluator.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int is_char_accepted_in_var_name(char symbol)
{
    int c = (int) symbol;
    return (
        (c >= 65 && c <= 90) ||
        (c >= 97 && c <= 122) ||
        (c >= 48 && c <= 57) ||
        c == 45 || c == 95
    );
}

int recognize_comment_statement(char* lineStr)
{
    int i = 0;
    int mode = 1;
    /*
    recognize pattern : ^\s*#.*$
    mode 1 any space
    mode 2 the start of the comment
    */
    while (lineStr[i] != '\0') {
        if (mode == 1) {
            if (lineStr[i] == '#') {
                mode = 2;
                continue;
            }
            if (lineStr[i] != ' ') break;
        }
        i++;
    }
    if (mode != 2) return 0;

    return 1;
}

// recognize in a string a keyword with a space before and a space afer OR the end of string after
/*
CASES:
[SPACE]NEEDLE$
[SPACE]NEEDLE[SPACE+]$

like
if machin then
while machin do
*/
int recognize_termination_keyword(char* needle, char* subject, int from)
{
    int needleLen = strlen(needle);
    int subLen = strlen(subject);
    int remainLen = subLen - from;

    if (remainLen < needleLen+1) {
        return 0;
    }
    char terminationCandidate[remainLen];
    str_extract((char*) &terminationCandidate, subject, from, remainLen);

    // we could directly integrate the trim space process without extracting the 

    char trimedTermination[remainLen];
    trim_space((char*) &trimedTermination, terminationCandidate);

    return strcmp(trimedTermination, needle) == 0;
}

// recognize "set VAR_NAME to EVALUATION"
int recognize_set_statement(char* lineStr, struct SetStatement* res)
{
    static char setStatementStartKeyword[] = SYNTAX_SET_START;
    static char setStatementStopKeyword[] = SYNTAX_SET_STOP;

    int i = 0;
    int mode = 1;
    int arg = 0;
    /*
    recognize pattern : ^\s*set ([a-zA-Z0-9_-]+) to (\S)+
    mode 1 any space
    mode 2 set
    mode 3 any space
    mode 4 var name
    mode 5 any space
    mode 6 to
    mode 7 any space
    mode 8 the expression
    */
    while (lineStr[i] != '\0') {
        //printf("mode: %d, char: '%c' \n", mode, lineStr[i]);
        if (mode == 1) {
            if (str_needle_at_pos(setStatementStartKeyword, lineStr, i)) {
                mode = 2;
                i += strlen(setStatementStartKeyword);
                continue;
            }
            if (lineStr[i] != ' ') break;
        }
        if (mode == 2) {
            if (arg > 0 && is_char_accepted_in_var_name(lineStr[i])) {
                mode = 3;
                arg = 0;
                res->name_start = i;
                i++;
                continue;
            }
            if (lineStr[i] != ' ') break;
            arg++;
        }
        if (mode == 3) {
            if (lineStr[i] == ' ') {
                mode = 4;
                arg = 0;
                res->name_stop = i;
                i++;
                continue;
            }
            if (!is_char_accepted_in_var_name(lineStr[i])) break;
        }
        if (mode == 4) {
            if (str_needle_at_pos(setStatementStopKeyword, lineStr, i)) {
                mode = 5;
                i += strlen(setStatementStopKeyword);
                continue;
            }
            break;
        }
        if (mode == 5) {
            if (arg == 0 && lineStr[i] != ' ') break;
            if (arg > 0 && lineStr[i] != ' ') {
                // success
                mode = 6;
                res->expression_start = i;
                break;
            }
            arg++;
        }

        i++;
    }

    if (mode != 6) return 0;
    return 1;
}

// recognize "if EXPRESSION then \n begin (...) \n end"
// FIXME: does no allow for inline if
// FIXME: allow for multiline if statement
int recognize_if_statement(char* lineStr, struct IfStatement* res)
{
    static char ifStatementStartKeyword[] = SYNTAX_IF_START;
    static char ifStatementStopKeyword[] = SYNTAX_IF_STOP;

    size_t len = strlen(lineStr);
    if (len < strlen(ifStatementStartKeyword) + 1 + strlen(ifStatementStopKeyword)) return 0;

    //printf("try to reco if statement \n");
    int i = 0;
    int mode = 1;
    int arg = 0;
    
    /*
    recognize pattern : ^\s*if {any string} then (\S)+
    mode 1 any space
    mode 2 "if"
    mode 3 any space
    mode 4 expression
    mode 5 any space
    mode 6 "then"
    mode 7 any space
    mode 8 expression
    if we reach the end of the line without getting anything we consider that we need smth after it
    multiple modes:
    */
    while (lineStr[i] != '\0') {
        if (mode == 1) {
            if (str_needle_at_pos(ifStatementStartKeyword, lineStr, i)) {
                mode = 2;
                i += strlen(ifStatementStartKeyword);
                continue;
            }
            if (lineStr[i] != ' ') break;
        }
        if (mode == 2) {
            if (arg > 0 && lineStr[i] != ' ') {
                mode = 3;
                arg = 0;
                res->expression_start = i;
                continue;
            }
            if (lineStr[i] != ' ') break;
            arg++;
        }
        if (mode == 3) {
            // in expression
            if (arg > 0 && recognize_termination_keyword(ifStatementStopKeyword, lineStr, i)) {
                // TODO: may be verify that the expression is valid?
                // if an expression contains " then" it will not work that well...
                // may be we need to verify that the rest of the line is empty
                mode = 4;
                arg = 0;
                res->expression_stop = i;
                continue;
            }
            arg++;
        }
        if (mode == 4) break;

        i++;
    }
    if (mode != 4) return 0;
    return 1;
}

// recognize "while EXPRESSION then \n begin (...) \n end"
int recognize_while_statement(char* lineStr, struct WhileStatement* res)
{
    static char whileStatementStartKeyword[] = SYNTAX_WHILE_START;
    static char whileStatementStopKeyword[] = SYNTAX_WHILE_STOP;
    
    size_t len = strlen(lineStr);
    if (len < strlen(whileStatementStartKeyword) + 1 + strlen(whileStatementStopKeyword)) return 0;

    //printf("try to reco if statement \n");
    int i = 0;
    int mode = 1;
    int arg = 0;

    while (lineStr[i] != '\0') {
        //printf("i: %d, mode: %d, char: '%c' \n", i, mode, lineStr[i]);
        if (mode == 1) {
            if (str_needle_at_pos(whileStatementStartKeyword, lineStr, i)) {
                mode = 2;
                i += strlen(whileStatementStartKeyword);
                continue;
            }
            if (lineStr[i] != ' ') break;
        }
        if (mode == 2) {
            if (arg > 0 && lineStr[i] != ' ') {
                mode = 3;
                arg = 0;
                res->expression_start = i;
                continue;
            }
            if (lineStr[i] != ' ') break;
            arg++;
        }
        if (mode == 3) {
            // in expression
            if (
                arg > 0 &&
                recognize_termination_keyword(whileStatementStopKeyword, lineStr, i)
            ) {
                mode = 4;
                arg = 0;
                res->expression_stop = i;
                continue;
            }
            arg++;
        }
        if (mode == 4) break;

        i++;
    }
    if (mode != 4) return 0;
    return 1;
}

/*
Return 1 if it found a single word with only spaces
basically it does this check:
trim_space(lineStr) == word
*/
int recognize_word(char* lineStr, char* word)
{
    int len = strlen(lineStr);
    int expectedLen = strlen(word);
    if (len < expectedLen) {
        return 0;
    }
    // trim spaces on both sides
    int start = 0;
    int end = len-1;
    while (lineStr[start] == ' ') {
        start++;
    }
    while (lineStr[end] == ' ') {
        end--;
    }
    end +=1;

    char dest[end-start];
    str_extract((char*) &dest, lineStr, start, end-start);

    return strcmp(dest, word) == 0;
}

// Will interpret a line
// the string must be inputed without a new line at the end
int process_line(struct StateContainer* state, char* str)
{
    if (LINE_PROCESSING_DEBUG_LEVEL >= 1) printf("\n ======= PROCESSING LINE '%s' =======\n", str);
    if (LINE_PROCESSING_DEBUG_LEVEL >= 2) printf("skipping: %d, blockStackLen: %d \n", state->skipping, state->blockStack->length);

    state->lastEvaluationType = TYPE_NULL;
    state->lastEvaluationResult = 0;

    int len = strlen(str);

    if (recognize_comment_statement(str)) {
        if (LINE_PROCESSING_DEBUG_LEVEL >= 2) printf("Comment recognized \n");
        state->linePtr++;
        return 1;
    }
    if (is_full_of_space(str)) {
        state->linePtr++;
        return 1;
    }

    if (recognize_word(str, SYNTAX_END)) {
        int lastBlockType;
        int_stack_pop(state->blockStack, &lastBlockType); // FIXME: use a stack with size byte instead of int
        if (!state->skipping && lastBlockType == BLOCK_WHILE) {
            int lastLoopLine;
            int_stack_pop(state->loopStack, &lastLoopLine);

            state->linePtr = lastLoopLine;
            return 1;
        }
        if (LINE_PROCESSING_DEBUG_LEVEL >= 2) printf("recognize end state->blockStackAnchor: %d \n", state->blockStackAnchor);
        if (state->skipping && int_stack_length(state->blockStack) == state->blockStackAnchor) {
            // end of skipping
            if (LINE_PROCESSING_DEBUG_LEVEL >= 2) printf("end of skipping \n");
            state->skipping = 0;
        }

        state->linePtr++;
        return 1;
    }

    struct IfStatement ifStatementParsing;
    byte ifRecognized = recognize_if_statement(str, &ifStatementParsing);

    struct WhileStatement whileStatementParsing;
    byte whileRecognized = recognize_while_statement(str, &whileStatementParsing);

    if (ifRecognized) {
        if (LINE_PROCESSING_DEBUG_LEVEL >= 2) printf("If statement recognized express (%d:%d)\n", ifStatementParsing.expression_start, ifStatementParsing.expression_stop);

        int_stack_push(state->blockStack, BLOCK_IF);
        if (state->skipping) {
            state->linePtr++;
            return 1;
        }

        // expression to evaluate
        int expressLen = ifStatementParsing.expression_stop - ifStatementParsing.expression_start;
        char dest[len];
        str_extract((char*) &dest, str, ifStatementParsing.expression_start, expressLen);
        // printf("expression to eval in if '%s' \n", dest);
        int res;
        byte resType;
        int evalStat = evaluate(state, (char*) &dest, &res, &resType);
        if (evalStat != 0) {
            printf("Syntax error for line \"%s\"\n", str);
            return 0;
        }
        
        byte doFollow = convert_to_bool(resType, res);
        if (LINE_PROCESSING_DEBUG_LEVEL >= 2) printf("Got %s, doFollow: %d \n", get_repr(resType, &res), doFollow);
        
        if (!doFollow) {
            state->blockStackAnchor = int_stack_length(state->blockStack)-1;
            state->skipping = 1;
        }
        
        state->linePtr++;
        return 1;
    }

    if (whileRecognized) {
        if (LINE_PROCESSING_DEBUG_LEVEL >= 2) printf("While statement recognized express (%d:%d)\n", whileStatementParsing.expression_start, whileStatementParsing.expression_stop);

        int_stack_push(state->blockStack, BLOCK_WHILE);
        if (state->skipping) {
            state->linePtr++;
            return 1;
        }

        // expression to evaluate
        int expressLen = whileStatementParsing.expression_stop - whileStatementParsing.expression_start;
        char dest[len];
        str_extract((char*) &dest, str, whileStatementParsing.expression_start, expressLen);
        // printf("expression to eval in if '%s' \n", dest);
        int res;
        byte resType;
        int evalStat = evaluate(state, (char*) &dest, &res, &resType);
        if (evalStat != 0) return 0;

        byte doFollow = convert_to_bool(resType, res);
        if (LINE_PROCESSING_DEBUG_LEVEL >= 2) printf("Got %s, doFollow: %d \n", get_repr(resType, &res), doFollow);

        if (doFollow)  {
            // follow the line
            int_stack_push(state->loopStack, state->linePtr);
        }
        if (!doFollow)  {
            state->blockStackAnchor = int_stack_length(state->blockStack)-1;
            state->skipping = 1;
        }
        
        state->linePtr++;
        return 1;
    }
    if (state->skipping) {
        // we've finish to analyse structure-aware blocks
        state->linePtr++;
        return 1;
    }

    if (recognize_word(str, SYNTAX_EXIT)) {
        // exit the whole script
        state->running = 0;
        return 1;
    }

    if (recognize_word(str, SYNTAX_CONTINUE)) {
        int lastLoopLine;
        if (!int_stack_pop(state->loopStack, &lastLoopLine)) {
            printf("Syntax error: unexpected continue, not in a loop.\n");
            return 0;
        }
        state->linePtr = lastLoopLine;

        // find the last while if the block stack and pop blocks up to that point
        int lastBlockType;
        while (lastBlockType != BLOCK_WHILE) {
            if (!int_stack_pop(state->blockStack, &lastBlockType)) {
                printf("Syntax error: unexpected continue.\n");
                return 0;
            }
        }
        int_stack_pop(state->blockStack, &lastBlockType);

        return 1;
    }

    if (recognize_word(str, SYNTAX_BREAK)) {
        int lastLoopLine;
        if (!int_stack_pop(state->loopStack, &lastLoopLine)) {
            printf("Syntax error: unexpected break, not in a loop.\n");
            return 0;
        }

        // go into skip mode
        // find the stack anchor coresponding to the last while
        // we will need to search in the block stack without poping
        int lastLoopAnchor = state->blockStack->length-1;
        while (lastLoopAnchor >= 0) {
            if (state->blockStack->data[lastLoopAnchor] == BLOCK_WHILE) break;
            lastLoopAnchor--;
        }
        state->blockStackAnchor = lastLoopAnchor;
        state->skipping = 1;
        state->linePtr++;
        return 1;
    }


    struct SetStatement setStatementParsing;
    if (recognize_set_statement(str, &setStatementParsing)) {
        if (LINE_PROCESSING_DEBUG_LEVEL >= 2) {
            printf("Set statement recognized (%d, %d, %d) \n",
                setStatementParsing.name_start,
                setStatementParsing.name_stop,
                setStatementParsing.expression_start);
        }
        
        // handle the set statement (set a variable)
        int nameLen = setStatementParsing.name_stop-setStatementParsing.name_start;
        char* name = (char*) malloc(sizeof(char) * (nameLen+1));
        for (int z = 0; z < nameLen; z++) {
            name[z] = str[setStatementParsing.name_start + z];
        }
        name[nameLen] = '\0';

        int res;
        byte resType;
        int expressLen = len - setStatementParsing.expression_start;
        char express[expressLen];
        str_extract((char*) &express, str, setStatementParsing.expression_start, expressLen);
        if (LINE_PROCESSING_DEBUG_LEVEL >= 2) printf("Got expression '%s' \n", express);

        int evalStat = evaluate(state, (char*) &express, &res, &resType);
        if (evalStat != 0) {
            printf("Error: could not evaluate the expression associated with the content of the variable to set \"%s\".\n", str);
            return 0;
        }
        
        if (LINE_PROCESSING_DEBUG_LEVEL >= 2) printf("==> Set '%s' to %s \n", name, get_repr(resType, &res));
        var_store_set(state->varStore, name, resType, &res);

        state->linePtr++;
        return 1;
    }

    // we evaluate the expression and we register it in the state in case of a REPL that want to get the result of the expression
    int res;
    byte resType;
    int evalStat = evaluate(state, str, &res, &resType);
    if (evalStat != 0) {
        printf("Error: could not evaluate expression \"%s\".\n", str);
        return 0;
    }

    state->lastEvaluationType = resType;
    state->lastEvaluationResult = res;
    state->linePtr++;
    return 1;
}


int process_script(struct StateContainer* state, char* script)
{
    int lineCount = 0;
    int i = 0;
    while (script[i] != '\0') {
        if (script[i] == '\n') {
            lineCount += 1;
        }
        i++;
    }
    if (LINE_PROCESSING_DEBUG_LEVEL >= 1) printf("\n ==================================================================== \n");
    if (LINE_PROCESSING_DEBUG_LEVEL >= 1) printf(" ======== NEW SCRIPT ======== \n");
    if (LINE_PROCESSING_DEBUG_LEVEL >= 1) printf(" ==================================================================== \n");
    if (LINE_PROCESSING_DEBUG_LEVEL >= 1) printf("line count: %d \n", lineCount);

    // FIXME: conditional line ptr reset
    state->linePtr = 0;

    int lineStarts[lineCount];
    int lineEnds[lineCount];
    lineStarts[0] = 0;

    i = 0;
    int lineIndex = 0;
    while (script[i] != '\0') {
        if (script[i] == '\n') {
            lineEnds[lineIndex] = i;
            //printf("end of a line: s: %d e: %d \n", lineStarts[lineIndex], lineEnds[lineIndex]);
            if (script[i+1] != '\0') {
                lineStarts[lineIndex+1] = i+1;
            }
            lineIndex++;
        }
        i++;
    }

    // for (int j = 0; j < lineCount; j++) {
    //     int lineLen = (lineEnds[j] - lineStarts[j]);
    //     char line[lineLen+1];
    //     str_extract(&line, script, lineStarts[j], lineLen);
    //     // printf("line %d %d \n", lineStarts[j], lineEnds[j]);
    //     // printf("line : '%s' \n", line);
    // }

    while (state->running && state->linePtr != lineCount) {
        if (state->linePtr >= lineCount) {
            printf("Error: Invalid line %d \n.", state->linePtr);
            break;
        }
        int lineLen = (lineEnds[state->linePtr] - lineStarts[state->linePtr]);
        char line[lineLen+1];
        str_extract((char*) &line, script, lineStarts[state->linePtr], lineLen);
        
        int stat = process_line(state, line);
        if (!stat) {
            printf("Error: Abnormal end of run at line %d: '%s'.\n", state->linePtr, line);
            return 0;
        }
    }

    if (LINE_PROCESSING_DEBUG_LEVEL >= 1) {
        // end of run
        printf("End of run dumping var_store \n");
        var_store_print(state->varStore);
        printf("=== end of dump\n");
    }

    return 1;
}
