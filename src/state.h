/**
This is where we store the state of our program
we store:
- the variables names and their values (for now only int)
- the functions names and their references
*/
#include "./utils.h"
#include "./stack.h"
#include "./var_store.h"
#ifndef STATE_H_
#define STATE_H_

struct StateContainer {
    struct VariableStore* varStore;

    // if true, continue to look for new lines to execute
    byte running;

    // the line we are currently processing
    int linePtr;

    // store the head address of loops
    struct IntStack* loopStack;
    
    // a stack that tracks the blocks we are in, 0 for a if, 1 for a while..
    struct IntStack* blockStack;

    // formely known as blockStackLengthBeforeSkip
    int blockStackAnchor;
    // if true tell the interpreter to skip until the block stack is at the blockStackLengthBeforeSkip
    byte skipping;
    
    // struct FunctionStore* funcStore;
    // struct StringStore* strStore ??

    int lastEvaluationResult;
    byte lastEvaluationType;
};

struct StateContainer* state_init();

#endif

/*
proto proof that just a if block counter is not sufficient to really track every if blocks

if true then -> la on dit que ifBlocks = 0
if false then -> la on dit que ifBlocks = 1
smth
if smth then ifBlocks = 2
end ifBlocks = 1
end ifBlocks = 0
end ifBlocks = -1 -> on peut pas retrouver le block d'avant le skip
conclusion: on ne peut pas juste utiliser un compteur à la con, ça ne marche pas il faut obligatoirement utiliser une stack
*/
