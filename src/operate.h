#ifndef OPERATOR_H_
#define OPERATOR_H_

#define OPERATE_DEBUG_LEVEL 0

int is_operator(char candidate);

int operate(
    unsigned char operator,
    unsigned char typeA, int aRepr,
    unsigned char typeB, int bRepr,
    unsigned char* typeRes, int* resPtr
);

#endif
