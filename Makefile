WERROR?=
CFLAGS=-Wall -Wextra $(WERROR) -pedantic -I.
CXXFLAGS_WITHOUT_PKGS=$(CFLAGS) -fno-exceptions -Wno-missing-braces -Wswitch-enum -lm

TEST_SRCS_ENC := $(foreach DIR,src,$(patsubst $(DIR)/%,%,$(wildcard ./src/*.c)))
TEST_SRCS_ENC := $(filter-out %main.c, $(TEST_SRCS_ENC))

build:
	cp src/config_env_main.h src/config_env.h
	gcc src/* -o ./bin/main ${CXXFLAGS_WITHOUT_PKGS}
test:
	cp src/config_env_main.h src/config_env.h
	gcc ${TEST_SRCS_ENC} ./tests/* -o ./bin/test ${CXXFLAGS_WITHOUT_PKGS}
	./bin/test
test-no-run: ./tests/*
	cp src/config_env_main.h src/config_env.h
	gcc ${TEST_SRCS_ENC} ./tests/* -o ./bin/test ${CXXFLAGS_WITHOUT_PKGS}
test-debug: ./tests/*
	cp src/config_env_main.h src/config_env.h
	gcc -g ${TEST_SRCS_ENC} ./tests/* -o ./bin/test ${CXXFLAGS_WITHOUT_PKGS}	
emscripten:
	cp wasm/config_env_wasm.h src/config_env.h
	emcc -O3 -s WASM=1 -s EXPORTED_RUNTIME_METHODS=ccall,cwrap,getValue,setValue \
	./wasm/emscripten.c ${TEST_SRCS_ENC} -o wasm/langatator_adapter.js
sandbox:
	gcc ${TEST_SRCS_ENC} ./sandbox.c -o ./bin/sandbox ${CXXFLAGS_WITHOUT_PKGS}
	./bin/sandbox
