'use strict';

// let app = document.getElementById("app");
// let wasm = null;
// let iota = 0;

// function cstrlen(mem, ptr) {
//     let len = 0;
//     while (mem[ptr] != 0) {
//         len++;
//         ptr++;
//     }
//     return len;
// }

// function cstr_by_ptr(mem_buffer, ptr) {
//     const mem = new Uint8Array(mem_buffer);
//     const len = cstrlen(mem, ptr);
//     const bytes = new Uint8Array(mem_buffer, ptr, len);
//     return new TextDecoder().decode(bytes);
// }

// function platform_panic(file_path_ptr, line, message_ptr) {
//     const buffer = wasm.instance.exports.memory.buffer;
//     const file_path = cstr_by_ptr(buffer, file_path_ptr);
//     const message = cstr_by_ptr(buffer, message_ptr);
//     console.error(file_path+":"+line+": "+message);
// }

// function platform_log(message_ptr) {
//     const buffer = wasm.instance.exports.memory.buffer;
//     const message = cstr_by_ptr(buffer, message_ptr);
//     console.log(message);
// }

// WebAssembly.instantiateStreaming(fetch('langatator.wasm'), {
//     env: {
//         platform_panic,
//         platform_log
//     }
// }).then((w) => {
//     wasm = w;

//     console.log(wasm.instance.exports.wasm_info());

//     //const buffer = wasm.instance.exports.memory.buffer;
// });

const consoleOutput = document.getElementById('console-output')

function log_intake(ptr, len) {
    let line = ""
    for (var i = 0; i < len; i++) {
        let val = Module.getValue(ptr+i, 'i8')
        line = line + String.fromCharCode(val)
    }
    console.log('Got line', line)

    // const pre= document.createElement('pre')
    // consoleOutput.appendChild(pre)
}


Module.onRuntimeInitialized = async _ => {
    const api = {
        init: Module.cwrap('wasm_init_state', 'number', []),
        processLine: Module.cwrap('wasm_process_line', 'number', ['number', 'string']),
        getTypeFromRef: Module.cwrap('wasm_get_type_from_ref', 'number', ['number']),
        getIntFromRef: Module.cwrap('wasm_get_int_from_ref', 'number', ['number']),

        // standalone (integrated state initialization)
        processScript: Module.cwrap('wasm_process_script', 'number', ['string']),
    };
    console.log(api);

    // const ref = api.init();
    // console.log(`Got ref: ${ref}`);
    
    window.processLine = (line) => {
        const resultRef = api.processLine(ref, line);
        console.log(`Got result ref: ${resultRef}`);
        console.log(api.getTypeFromRef(resultRef));
        console.log(api.getIntFromRef(resultRef));
    }

    window.submitScript = () => {
        const textarea = document.getElementById('script-input')
        api.processScript(textarea.value)
    }



};
