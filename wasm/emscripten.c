#include "emscripten.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

EM_JS(void, log_to_js, (char* buff, size_t len), {
    log_intake(buff, len);
});

int printf_wrap(const char* format, ...) {
    va_list args;
    va_start(args, format);
    char* out;
    vsprintf(out, format, args);
    char* sharedPos = (char*) malloc(strlen(out) * sizeof(char));
    strcpy(sharedPos, out);
    log_to_js((void*) sharedPos, strlen(out));
    va_end(args);
    return 1;
}

#include "../src/config.h"
#include "../src/utils.h"
#include "../src/types.h"
#include "../src/state.h"
#include "../src/line_processing.h"

struct Result {
    byte type;
    int data;
};

EMSCRIPTEN_KEEPALIVE
void* wasm_init_state()
{
    struct StateContainer* ptr = state_init();
    return (void*) ptr;
}

EMSCRIPTEN_KEEPALIVE
void* wasm_process_line(void* state, char* line)
{
    struct Result* res = malloc(sizeof(struct Result));

    process_line(state, line);

    res->type = ((struct StateContainer*) state)->lastEvaluationType;
    res->data = ((struct StateContainer*) state)->lastEvaluationResult;

    return (void*) res;
}

EMSCRIPTEN_KEEPALIVE
void wasm_process_script(char* scriptLines)
{
    printf_wrap("Will process script \"%s\" \n", scriptLines);
    struct StateContainer* state = state_init();
    process_script(state, scriptLines);
}

// very dangerous indeed
EMSCRIPTEN_KEEPALIVE
int wasm_get_type_from_ref(void* ref) {
    return ((struct Result*) ref)->type;
}

EMSCRIPTEN_KEEPALIVE
int wasm_get_int_from_ref(void* ref) {
    return ((struct Result*) ref)->data;
}

// EMSCRIPTEN_KEEPALIVE
// int wasm_info(char* lines)
// {
//     struct StateContainer* state = state_init();
//     process_script(state, lines);

//     return state->lastEvaluationResult;
// }

