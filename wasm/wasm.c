#include <stdlib.h>
#include "../src/config.h"
#define CONFIG_PRINT_METHOD PRINT_METHOD_WASM
#include "../src/utils.h"
#include "../src/types.h"
#include "../src/state.h"
#include "../src/line_processing.h"

void* wasm_init_state()
{
    struct StateContainer* ptr = state_init();
    return (void*) ptr;
}

void* wasm_process_line(void* state, char* line)
{
    struct Result* res = malloc(sizeof(struct Result));

    process_line(state, line);

    res->type = ((struct StateContainer*) state)->lastEvaluationType;
    res->data = ((struct StateContainer*) state)->lastEvaluationResult;

    return (void*) res;
}

// very dangerous indeed
int wasm_get_type_from_ref(void* ref) {
    return ((struct Result*) ref)->type;
}

int wasm_get_int_from_ref(void* ref) {
    return ((struct Result*) ref)->data;
}

// EMSCRIPTEN_KEEPALIVE
// int wasm_info(char* lines)
// {
//     struct StateContainer* state = state_init();
//     process_script(state, lines);

//     return state->lastEvaluationResult;
// }

