// #include <stdio.h>
// #include "./src/types.h"
// #include "./src/list.h"
// #include "./src/number_parsing.h"
// #include "./src/funcs.h"
// #include "./src/utils.h"
// #include "./src/var_store.h"
// #include "./src/line_processing.h"
// #include "./src/state.h"
// #include <stdlib.h>
// #include <sys/time.h>
// #include <stddef.h>
#include <stdio.h>

// #define printf_wrap(f_, ...)                                                                            \
// {                                                                                                 \
//     printf_wrap("T=%d \t", (unsigned) time(NULL)); \
//     printf_wrap((f_), ##__VA_ARGS__);                                                                  \
// };

#define eprintf_wrap(template, ...) printf_wrap(template, ##__VA_ARGS__);


int main () {

    eprintf_wrap("Hello %d %s \n", 13, "World");
    // struct VariableStore* store = var_store_init();
    // printf_wrap("%d", var_store_hash_name(store, "print_number index"));

    // int i = -1;
    // if (i < 0) {
    //     printf_wrap("lol jpp \n");
    // }
    
    return 0;
    //struct List l1;

    // short val = 17;
    // list_set(&l1, 0, TYPE_VAR_NAME, &val);
    // list_print(&l1);

    // return 0;
    
    // int yes = 1234;
    // int dst = 0;
    // memcpy(&dst, &yes, 4);
    // printf_wrap("yes: %d \n", dst);

    //struct VariableStore* store = var_store_init();

    // int hashRes = var_store_hash_name(store, "HelloWorld++");
    // printf_wrap("hashRes: %d \n", hashRes);
    // int val = 1234;
    // var_store_set(store, "foo", TYPE_INT, &val);
    // var_store_get_key(store, "while i < 10 then");

    //printf_wrap("Pos of var: %d \n", var_store_get_pos(store, "foo"));

    //byte type = var_store_get_type(store, "foo");
    //printf_wrap("Type of var: %d \n", type);
    
    //int key = var_store_get_pos(store, "foo");
    //printf_wrap("size of type %d \n", get_size_of_type(store->container[key].type));
    //printf_wrap("%d \n", *((int*) store->container[key].dataPtr));

    // int val2 = 0;
    // var_store_copy(store, "foo", &val2);

    // printf_wrap("Value of var: %d \n", val2);

    // printf_wrap("==== \n");
    // printf_wrap("==== \n");

    // char* lines = "# hello world\n"
    //     "set x to 5\n"
    //     "set y to 1\n"
    //     "if !(x+y = 6) then\n"
    //     "   print_number(x+y)\n"
    //     "end\n"
    //     "\n";

    // char* lines1 = "set x to 5\n"
    //     "set y to 1\n"
    //     "if !(x+y = 6) then\n"
    //     "   print_number(x+y)\n"
    //     "end\n"
    //     "\n";

    // printf_wrap("%s", lines1);
  
    // struct StateContainer* state = state_init();
    // process_script(state, lines1);

    // struct List l1;

    // list_append_int(&l1, 4);
    // list_append_char(&l1, '*');
    // list_append_int(&l1, 5);

    // list_print(&l1);

    // list_delete(&l1, 0);

    // list_print(&l1);

    // list_delete(&l1, 0);

    // list_print(&l1);

    // float res = 0;

    // void* ptr = &res;

    // printf_wrap("%d\n", sizeof(ptr));
    // int found = identify_func_name("ABS");
    // printf_wrap("found: %d \n", found);

    // unsigned char argsType[1] = { TYPE_FLOAT };
    // int argsVals[1] = { get_int_rep_from_float(-3.145) };
    // int resVal = 0;
    // unsigned char resType = 0;
    // execute_func(found, 1, argsType, argsVals, &resVal, &resType);
    // printf_wrap("func res type: %d \n", resType);
    // printf_wrap("func res: %f \n", get_float_from_int_rep(resVal));
    
    // int stat = parse_float("1052.254", &res);
    // printf_wrap("float parsing stat: %d \n", stat);
    // printf_wrap("final float: %f \n", res);
}

